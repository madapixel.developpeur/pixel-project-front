import { CreateProjectComponent } from './create-project/create-project.component';
import { CreateProjectComponent as CreateProjectComponentAdmin } from './admin/atelier/create-project/create-project.component';
import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CreateComponent } from './test/component/lesson/create/create.component';
import { UpdateComponent } from './test/component/lesson/update/update.component';
import { ListComponent } from './test/component/lesson/list/list.component';
import { LoginClientComponent } from './client/pages/auth/login-client/login-client.component';
import { SigninClientComponent } from './client/pages/auth/signin-client/signin-client.component';
import { DragDropComponent } from './test/component/drag-drop/drag-drop.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AuthenticationInterceptor } from './interceptors/authentication.interceptor';
import { SharedModule } from './commons/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SwiperModule } from 'swiper/angular';
import { FileUploadModule } from 'ng2-file-upload';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MultiStepFormComponent } from './components/multi-step-form/multi-step-form.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { HomeComponent } from './commons/pages/home/home.component';
import { ChatboxComponent } from './components/chatbox/chatbox.component';
registerLocaleData(localeFr);
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  declarations: [
    AppComponent,
    CreateComponent,
    UpdateComponent,
    ListComponent,
    HomeComponent,
    DragDropComponent,
    CreateProjectComponent,
    MultiStepFormComponent,
    CreateProjectComponentAdmin,
    ChatboxComponent
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    DragDropModule,
    CKEditorModule,
    SwiperModule,
    FileUploadModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    { provide: LOCALE_ID, useValue: 'fr-FR'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
