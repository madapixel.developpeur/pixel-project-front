import { CreateProjectDetailsComponent } from './create-project-details/create-project-details.component';
import { CreateProjectComponent } from './create-project.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path:"",
    component: CreateProjectComponent,
    children:[
      {
        path: "details/:projectId",
        component: CreateProjectDetailsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateProjectRoutingModule { }
