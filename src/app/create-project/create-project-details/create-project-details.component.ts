import { FormData } from './../../model/form-data';

import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { MessageService } from 'src/app/commons/services/message.service';
import { lastValueFrom } from 'rxjs';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from 'src/app/services/file.service';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from 'src/app/services/user.service';
import { environment } from '@env/environment';
@Component({
  selector: 'app-create-project-details',
  templateUrl: './create-project-details.component.html',
  styleUrls: ['./create-project-details.component.scss']
})
export class CreateProjectDetailsComponent implements OnInit {

  projectId : any = null;
  constructor(
    private route : ActivatedRoute,
    ) { 
      

    }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.paramMap.get("projectId");
    
    
    
    
  }
  
  


  
}
