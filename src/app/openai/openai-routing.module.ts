import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpenaiComponent } from './openai.component';
import { GenerateTextComponent } from '../commons/components/generate-text/generate-text.component';
import { GenerateImagesComponent } from '../commons/components/generate-images/generate-images.component';

const routes: Routes = [{
  path:"",
  component: OpenaiComponent,
  children:[
    {
      path: "",
      component: GenerateTextComponent
    },
    {
      path: "images",
      component: GenerateImagesComponent
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpenaiRoutingModule { }
