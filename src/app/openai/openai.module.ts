import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OpenaiComponent } from './openai.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../commons/shared.module';
import { OpenaiRoutingModule } from './openai-routing.module';



@NgModule({
  declarations: [
    OpenaiComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    OpenaiRoutingModule
  ]
})
export class OpenaiModule { }
