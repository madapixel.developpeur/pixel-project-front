import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-openai',
  templateUrl: './openai.component.html',
  styleUrls: ['./openai.component.scss']
})
export class OpenaiComponent {
  

  tabs : any[] = [
    {titre:'Générer un texte', route: '', icon: '<i class="fa fa-align-center font-medium-3 me-50" aria-hidden="true"></i>'},
    {titre:'Générer des images', route: 'images', icon: '<i class="fa fa-file-image font-medium-3 me-50" aria-hidden="true"></i>'}
  ];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getActiveRoute(){
    for(let i=1; i<this.tabs.length; i++){
      if(this.router.url.endsWith('/'+this.tabs[i].route)) return i;
    }
    return 0;
  }
}
