import { FormlyFieldConfig } from '@ngx-formly/core';
export class FormData {
    CATEGORY : string = "";
    TITLE : string = "";
    FORM_CONFIG : FormlyFieldConfig[] = [];
    MODEL : any = {};
}
