import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '@env/environment';
import { lastValueFrom } from 'rxjs';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent   {
  isLoading: boolean = false;

  form: FormGroup
  constructor(private fb: FormBuilder, private userService: UserService, private message: MessageService, private router: Router, private projectService: ProjectService) {
    this.form = fb.group({
      'email': [null, [Validators.required]],
      'password': [null, [Validators.required]],
    })
   }

   async submit(){
    this.isLoading = true;
    try{
      const data = await lastValueFrom(this.userService.login(this.form.value));
      
      if(data.user.roleId == '1'){
        const options = {filter: {}, pagination: {page: 1, pageElmtCount: 1, orderBy: [{column: 'creationDate', order: 'desc'}]}}
        const result: any = await lastValueFrom(this.projectService.getProjectsForClient(flattenObject(options)));
        if(result.data.length > 0) {
          
          if(result.data[0].status == environment.projectStatus.created){
            await this.router.navigateByUrl("/create-project/details/"+result.data[0]._id);
          } else{
            await this.router.navigateByUrl("/client/project/"+result.data[0]._id);
          }
          return; 
        }
      }
      const nextPath = this.userService.getNextLink(data.user.roleId);
      await this.router.navigate([nextPath]);
    }catch(e: any){
      console.log(e);
      this.message.showError(e);
    }
    this.isLoading = false;
   }
 

}
