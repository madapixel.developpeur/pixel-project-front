
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AtelierModule } from './admin/atelier/atelier.module';
import { ClientModule } from './client/client.module';
import { AccessDeniedComponent } from './commons/pages/access-denied/access-denied.component';
import { CreateProjectModule } from './create-project/create-project.module';
import { CreateProjectModule as CreateProjectModuleAdmin } from './admin/atelier/create-project/create-project.module';
import { AuthGuard } from './guards/auth.guard';
import { DragDropComponent } from './test/component/drag-drop/drag-drop.component';
import { CreateComponent } from './test/component/lesson/create/create.component';
import { ListComponent } from './test/component/lesson/list/list.component';
import { UpdateComponent } from './test/component/lesson/update/update.component';
import { UserModule } from './user/user.module';
import { HomeComponent } from './commons/pages/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'lesson/list', component: ListComponent },
  { path: 'lesson/create', component: CreateComponent },
  { path: 'lesson/update/:name', component: UpdateComponent },
  { path: 'drag-drop', component: DragDropComponent },

  //Client
  {
    path: 'client',
    loadChildren: () => import("./client/client.module").then((m) => ClientModule),
    canActivate: [AuthGuard],
    data: { roleIds: [1] }
  },
  //Client : create-project
  {
    path: "create-project",
    loadChildren: () => import("./create-project/create-project.module").then((m) => CreateProjectModule),
    canActivate: [AuthGuard],
    data: { roleIds: [1] }
  },
  {
    path: "create-project-admin",
    loadChildren: () => import("./admin/atelier/create-project/create-project.module").then((m) => CreateProjectModuleAdmin),
    canActivate: [AuthGuard],
    data: { roleIds: [2] }
  },
  //User
  {
    path: 'users', loadChildren: () => import("./user/user.module").then((m) => UserModule)
  },
  //Responsable atelier
  {
    path: 'admin/atelier',
    loadChildren: () => import("./admin/atelier/atelier.module").then((m) => AtelierModule),
    canActivate: [AuthGuard],
    data: { roleIds: [2] }
  },

  { path: 'forbidden', component: AccessDeniedComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})
export class AppRoutingModule { }
