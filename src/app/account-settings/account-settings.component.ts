import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {


  tabs : any[] = [
    {titre:'Informations générales', route: '', icon: '<i class="fa fa-user" class="font-medium-3 me-50"></i>'},
    {titre:'Mot de passe', route: 'password', icon: '<i class="fa fa-lock" class="font-medium-3 me-50"></i>'}
  ];

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  getActiveRoute(){
    for(let i=1; i<this.tabs.length; i++){
      if(this.router.url.endsWith('/'+this.tabs[i].route)) return i;
    }
    return 0;
  }


}
