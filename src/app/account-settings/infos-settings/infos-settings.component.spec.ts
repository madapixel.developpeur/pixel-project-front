import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfosSettingsComponent } from './infos-settings.component';

describe('InfosSettingsComponent', () => {
  let component: InfosSettingsComponent;
  let fixture: ComponentFixture<InfosSettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfosSettingsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InfosSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
