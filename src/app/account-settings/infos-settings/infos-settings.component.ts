import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-infos-settings',
  templateUrl: './infos-settings.component.html',
  styleUrls: ['./infos-settings.component.scss']
})
export class InfosSettingsComponent implements OnInit {

  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  user: any = null;
  form: FormGroup;
  selectedImageFile: File | null = null;
  imageUrl: any = null;


  constructor(private userService : UserService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private router : Router,
    private fileService: FileService,
    private fb: FormBuilder,
    public sanitizer: DomSanitizer) { 
      this.form = fb.group({
        'firstName': [null, [Validators.required]],
        'lastName': [null, [Validators.required]],
        'email': [null, [Validators.required]],
      });
    }

  ngOnInit(): void {
    this.fetchData();
  }

  async fetchData(){
    this.isLoading = true;
    try {
      this.user = await lastValueFrom<any>(this.userService.getUserData());
      this.form.setValue({
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email
      });
      this.resetImage();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }

  async submit(){
    this.isSubmitLoading = true;
    try {
      const values = this.form.value;
      values.image = this.user.image;
      if(this.selectedImageFile){
        values.image = await lastValueFrom(this.fileService.uploadFile(this.selectedImageFile));
      }
      await lastValueFrom(this.userService.updateUserInfos(values));
      this.messageService.showSuccess('Informations modifiées avec succès');
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isSubmitLoading = false;
  }

  
  resetImage(){
    this.selectedImageFile = null;
    this.imageUrl = this.user?.image ? this.fileService.getFileUrl(this.user.image) : null;
  }

  onImageSelected(event: any){
    
    if(event.target.files.length > 0 && event.target.files[0])  {
      this.selectedImageFile = event.target.files[0];
      this.fileService.toBase64(this.selectedImageFile).then((url: any) => this.imageUrl = url);
    }
  }

  

  getDefaultAvatarUrl(){
    return FileService.defaultAvatarUrl;
  }
  
}
