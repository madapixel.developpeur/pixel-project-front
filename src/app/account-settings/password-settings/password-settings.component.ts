import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-password-settings',
  templateUrl: './password-settings.component.html',
  styleUrls: ['./password-settings.component.scss']
})
export class PasswordSettingsComponent implements OnInit {

  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  form: FormGroup;


  constructor(private userService : UserService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private router : Router,
    private fb: FormBuilder,
    ) { 
      this.form = fb.group({
        'currentPassword': [null, [Validators.required]],
        'newPassword': [null, [Validators.required]],
        'confirmNewPassword': [null, [Validators.required]],
      });
    }

  ngOnInit(): void {
    
  }

  

  async submit(){
    this.isSubmitLoading = true;
    try {
      const values = this.form.value;
      await lastValueFrom(this.userService.updateUserPassword(values));
      this.messageService.showSuccess('Mot de passe modifié avec succès');
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isSubmitLoading = false;
  }

  
  

}
