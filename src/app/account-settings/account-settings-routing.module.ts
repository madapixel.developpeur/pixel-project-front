import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountSettingsComponent } from './account-settings.component';
import { InfosSettingsComponent } from './infos-settings/infos-settings.component';
import { PasswordSettingsComponent } from './password-settings/password-settings.component';

const routes: Routes = [{
  path:"",
  component: AccountSettingsComponent,
  children:[
    {
      path: "",
      component: InfosSettingsComponent
    },
    {
      path: "password",
      component: PasswordSettingsComponent
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountSettingsRoutingModule { }
