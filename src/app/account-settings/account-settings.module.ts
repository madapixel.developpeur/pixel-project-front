import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountSettingsRoutingModule } from './account-settings-routing.module';
import { AccountSettingsComponent } from './account-settings.component';
import { InfosSettingsComponent } from './infos-settings/infos-settings.component';
import { PasswordSettingsComponent } from './password-settings/password-settings.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../commons/shared.module';


@NgModule({
  declarations: [
    AccountSettingsComponent,
    InfosSettingsComponent,
    PasswordSettingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AccountSettingsRoutingModule
  ]
})
export class AccountSettingsModule { }
