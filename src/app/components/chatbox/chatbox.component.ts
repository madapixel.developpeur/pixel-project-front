import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'src/app/commons/services/message.service';
import { ChatService } from 'src/app/services/chat.service';
import { FileService } from 'src/app/services/file.service';
import { UserService } from 'src/app/services/user.service';
declare var $:any;

@Component({
  selector: 'app-chatbox',
  templateUrl: './chatbox.component.html',
  styleUrls: ['./chatbox.component.scss']
})
export class ChatboxComponent implements AfterViewInit, OnInit{


  userData : any;
  messages: any[] = [];
  form: FormGroup;

  constructor(private chatService: ChatService, 
    private userService: UserService,
    private fileService: FileService,
    private messageService: MessageService,
    private fb: FormBuilder){
      this.form = fb.group({
        message: [null, Validators.required]
      });

  }
  ngOnInit(): void {
    this.chatService.openAIResponse$.subscribe({
      next: (response: any) => {
        if(response.type == ChatService.OPENAI_MESSAGE_CHATBOT){
          if(response.error){
            this.messageService.showError(response.message);
          } else{
            this.messages.push({role: 'assistant', message: response.content})
          }
          
        }
      }
    })
    this.userService.userUpdate.subscribe({
      next: (result: any) => {
        this.userData = result;
      }
    });
  }

  ngAfterViewInit(): void {
    let $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });
        
        $chatbox.on('transitionend', function() {
            if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
        });
  }

  sendMessage(){
    const message = this.form.value.message;
    this.chatService.openAISendMessage({type: ChatService.OPENAI_MESSAGE_CHATBOT, req: message});
    this.form.reset();
    this.messages.push({role: 'user', message})
  }

  getAvatarUrl(chemin: string | null){
    return  chemin ? this.fileService.getFileUrl(chemin) : FileService.defaultAvatarUrl;
  }

}
