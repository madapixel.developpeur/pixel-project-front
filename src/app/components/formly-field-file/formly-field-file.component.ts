import { FileService } from 'src/app/services/file.service';
import { ProjectService } from './../../services/project.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FieldTypeConfig } from '@ngx-formly/core';
import { FieldType } from '@ngx-formly/material';

@Component({
  selector: 'formly-field-file',
  templateUrl: './formly-field-file.component.html',
  styleUrls: ["./formly-field-file.component.scss"]
})
export class FormlyFieldFileComponent extends FieldType<FieldTypeConfig>  implements OnInit{
  @ViewChild("fileinput") el: ElementRef;
  selectedFiles: File[] = [];
  constructor(public sanitizer: DomSanitizer, private fileService : FileService) {
    super();
  }
  ngOnInit(): void {
    this.initFiles();
  }
  async initFiles(){
    if(this.value == null) return;
    let filePaths : string[] = this.value;
    for(let i=0; i<filePaths.length; i++){
      let filename : string = `Fichier n° ${i}`;
      let blob : any = await this.fileService.getBlob(filePaths[i]);
      console.log(blob);
      this.selectedFiles.push(new File([blob], filename));
    }


    console.log(this.selectedFiles);
  }
  openFileInput() {
    this.el.nativeElement.click();
  }
  onDelete(index : number) {
    // this.formControl.reset();
    console.log(this.selectedFiles);
    this.selectedFiles.splice(index, 1);

    this.formControl.patchValue(this.selectedFiles);
    console.log("Form Control Value", this.formControl.value);
  }
  onChange(event : any) {
    this.selectedFiles = Array.from(event.target.files);
    console.log(this.selectedFiles);
  }
  getSanitizedImageUrl(file: File) {
    return this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(file)
    );
  }
  isImage(file: File): boolean {
    return /^image\//.test(file.type);
  }
}
