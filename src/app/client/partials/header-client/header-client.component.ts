import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { ChatService } from 'src/app/services/chat.service';
import { FileService } from 'src/app/services/file.service';
import { UserService } from 'src/app/services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-header-client',
  templateUrl: './header-client.component.html',
  styleUrls: ['./header-client.component.scss']
})
export class HeaderClientComponent implements OnInit {
  userData : any;
  env : any = environment;
  newMessages: any[] = [];

  constructor(
    private userService : UserService,
    private route : ActivatedRoute,
    private router : Router,
    private messageService: MessageService,
    private chatService: ChatService,
    private fileService: FileService
    ) { }

  ngOnInit(): void {
    this.userService.userUpdate.subscribe({
      next: (result: any) => {
        this.userData = result;
      }
    });
    window.addEventListener(ChatService.EVENT_NAME, (event: any) => {
      this.addNewMessage(event.detail);
    });
    window.addEventListener(ChatService.SEEN_EVENT_NAME, (event: any) => {
      this.setSeenChatUser(event.detail);
    });
    this.getNewMessages();
  }

  addNewMessage(message: any){
    if(message.userToId == this.userData._id){
      const index = this.newMessages.findIndex((elmt: any) => elmt._id == message.userFromId);
      let ms = null;
      if(index >= 0){
        ms = this.newMessages[index];
        ms.message = message;
        this.newMessages.splice(index, 1);
        
      } else{
        ms = {_id: message.userFromId, message};
      }
      this.newMessages.unshift(ms);
    }
  }

  setSeenChatUser(seen: any){
    const index = this.newMessages.findIndex((elmt: any) => elmt._id == seen.userToId);
    if(index >= 0){
      this.newMessages.splice(index, 1);
    }
  }

  async logout(){
    const data = await lastValueFrom(this.userService.logout());
    this.router.navigate(['/users/login']);
  }


  async getNewMessages(){
    try{
      const res: any = await lastValueFrom(this.chatService.getMessagesNotSeen());
      this.newMessages = res.data;
    } catch(err: any){
      console.error(err);
      this.messageService.showError(err);
    }
  }

  getAvatarUrl(chemin: string | null){
    return  chemin ? this.fileService.getFileUrl(chemin) : FileService.defaultAvatarUrl;
  }

}
