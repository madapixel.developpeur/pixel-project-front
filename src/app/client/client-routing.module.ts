
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountSettingsModule } from '../account-settings/account-settings.module';
import { ClientComponent } from './client.component';
import { ChatClientComponent } from './pages/chat-client/chat-client.component';
import { CreateProjectClientComponent } from './pages/create-project-client/create-project-client.component';
import { ProjectBillingComponent } from './pages/project-billing/project-billing.component';
import { ProjectDetailsClientComponent } from './pages/project-details-client/project-details-client.component';
import { ProjectListClientComponent } from './pages/project-list-client/project-list-client.component';
import { ProjectProgressionClientComponent } from './pages/project-progression-client/project-progression-client.component';
import { TodoListClientComponent } from './pages/todo-list-client/todo-list-client.component';

const routes: Routes = [
  {
    path:"",
    component: ClientComponent,
    children:[
      {
        path: "",
        component: CreateProjectClientComponent
      },
      {
        path: "project/:id",
        component: ProjectDetailsClientComponent
      },
      {
        path: "project/:projectId/todos",
        component: TodoListClientComponent
      },
      {
        path: "project",
        component: ProjectListClientComponent
      },
      {
        path: "project/:projectId/progression",
        component: ProjectProgressionClientComponent
      },
      {
        path: "project/:projectId/billing",
        component: ProjectBillingComponent
      },
      {
        path: "chat",
        component: ChatClientComponent
      },
      { 
        path : 'account-settings', 
        loadChildren: () => import("src/app/account-settings/account-settings.module").then((m) => AccountSettingsModule) ,
      },
      
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
