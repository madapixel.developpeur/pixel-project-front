import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module'; 
import { HeaderClientComponent } from './partials/header-client/header-client.component';
import { NavClientComponent } from './partials/nav-client/nav-client.component';
import { ClientComponent } from './client.component';
import { LoginClientComponent } from './pages/auth/login-client/login-client.component';
import { SigninClientComponent } from './pages/auth/signin-client/signin-client.component';
import { SharedModule } from '../commons/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CreateProjectClientComponent } from './pages/create-project-client/create-project-client.component';
import { SwiperModule } from 'swiper/angular';
import { ProjectDetailsClientComponent } from './pages/project-details-client/project-details-client.component';
import { TodoFilterClientComponent } from './components/todo-filter-client/todo-filter-client.component';
import { TodoListClientComponent } from './pages/todo-list-client/todo-list-client.component';
import { ProjectListClientComponent } from './pages/project-list-client/project-list-client.component';
import { ProjectFilterClientComponent } from './components/project-filter-client/project-filter-client.component';
import { ProjectProgressionClientComponent } from './pages/project-progression-client/project-progression-client.component';
import { ProjectBillingComponent } from './pages/project-billing/project-billing.component';
import { SignboxComponent } from './components/signbox/signbox.component';
import { ChatClientComponent } from './pages/chat-client/chat-client.component';
import { AccountSettingsModule } from '../account-settings/account-settings.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyFieldFileComponent } from '../components/formly-field-file/formly-field-file.component';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyMatInputModule } from '@ngx-formly/material/input';
import { FormlyMatCheckboxModule } from '@ngx-formly/material/checkbox';
import { FormlyMatFormFieldModule } from '@ngx-formly/material/form-field';
import { FormlyMatNativeSelectModule } from '@ngx-formly/material/native-select';
import { FormlyMatRadioModule } from '@ngx-formly/material/radio';
import { FormlyMatMultiCheckboxModule } from '@ngx-formly/material/multicheckbox';
import { FormlyMatSelectModule } from '@ngx-formly/material/select';
import { FormlyMatSliderModule } from '@ngx-formly/material/slider';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { FormlyMatTextAreaModule } from '@ngx-formly/material/textarea';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { NgImageSliderModule } from 'ng-image-slider';
@NgModule({
  declarations: [ 
    HeaderClientComponent,
    NavClientComponent,
    ClientComponent,
    LoginClientComponent,
    SigninClientComponent,
    CreateProjectClientComponent,
    ProjectDetailsClientComponent,
    TodoFilterClientComponent,
    TodoListClientComponent,
    ProjectListClientComponent,
    ProjectFilterClientComponent,
    ProjectProgressionClientComponent,
    ProjectBillingComponent,
    SignboxComponent,
    ChatClientComponent
    
  ],
  

  imports: [
    SharedModule,
    CommonModule,
    ClientRoutingModule,
    FormsModule,
    DragDropModule,
    ReactiveFormsModule,
    SwiperModule,
    NgbModule,
    PerfectScrollbarModule,
    SwiperModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      types: [{ name: 'file', component: FormlyFieldFileComponent, wrappers: ['form-field'] }],
    }),
    FormlyBootstrapModule,
    FormlyMatInputModule,
    FormlyMatCheckboxModule,
    FormlyMatFormFieldModule,
    FormlyMatNativeSelectModule,
    FormlyMatRadioModule,
    FormlyMatMultiCheckboxModule,
    FormlyMatSelectModule,
    FormlyMatSliderModule,
    FormlyMatDatepickerModule,
    FormlyMatTextAreaModule,
    FormlyMaterialModule, // Do not forget to import this !!
    NgxImageZoomModule,
    NgImageSliderModule
  ],
  
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ClientModule { }
