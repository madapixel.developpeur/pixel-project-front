import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectFilterClientComponent } from './project-filter-client.component';

describe('ProjectFilterClientComponent', () => {
  let component: ProjectFilterClientComponent;
  let fixture: ComponentFixture<ProjectFilterClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectFilterClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectFilterClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
