import { Component, EventEmitter, Output } from '@angular/core';
import { environment } from '@env/environment';

@Component({
  selector: 'app-project-filter-client',
  templateUrl: './project-filter-client.component.html',
  styleUrls: ['./project-filter-client.component.scss']
})
export class ProjectFilterClientComponent {

  projectStatus = environment.projectStatus;
  @Output() filterClicked = new EventEmitter<any>(true);

  filterValues: any = {
    name: '',
    status: '',
    dateMin: '',
    dateMax: '',
  }

  generateFilter(){
    const ans: any = [];
    if(this.filterValues.name) {
      ans.push({
        column: 'answers.categories.INFORMATIONS_SUPPLEMENTAIRES.nom',
        comparator: 'like',
        value: this.filterValues.name,
        type: 'string'
      })
    }

    if(this.filterValues.status !== '') {
      ans.push({
        column: 'status',
        comparator: '=',
        value: this.filterValues.status,
        type: 'int'
      })
    }

    if(this.filterValues.dateMin) {
      ans.push({
        column: 'creationDate',
        comparator: '>=',
        value: this.filterValues.dateMin,
        type: 'float'
      })
    }
    if(this.filterValues.dateMax) {
      ans.push({
        column: 'creationDate',
        comparator: '<=',
        value: this.filterValues.dateMax,
        type: 'float'
      })
    }
    
    return ans;
  }

  constructor() { }

 clickFilter(){
  console.log(this.filterValues)
  this.filterClicked.emit(this.generateFilter());
 }
 

 init(){
  this.filterValues = {
    name: '',
    status: '',
    dateMin: '',
    dateMax: '',
  }
  }

}
