import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
declare var $:any;
import "jsignature";
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-signbox',
  templateUrl: './signbox.component.html',
  styleUrls: ['./signbox.component.scss']
})
export class SignboxComponent implements OnInit, AfterViewInit {

  @Input() isSaveLoading: boolean = false;
  @Output() saveClicked = new EventEmitter<any>(true);
  @ViewChild('signbox') signboxElmtRef:ElementRef;
  
  constructor(
    private messageService: MessageService,
    private fileService: FileService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    $(this.signboxElmtRef.nativeElement).jSignature({'UndoButton':true});
  }

  clicked(){
    const data = $(this.signboxElmtRef.nativeElement).jSignature('getData', 'default');
    this.saveClicked.emit(data);
  }

  async importData(file: any){
    try{
      const data = await this.fileService.toBase64(file);
      this.clear();
      $(this.signboxElmtRef.nativeElement).jSignature('importData', data);
    } catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    } 
    
  }

  clear(){
    $(this.signboxElmtRef.nativeElement).jSignature('clear');
  }
}
