import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignboxComponent } from './signbox.component';

describe('SignboxComponent', () => {
  let component: SignboxComponent;
  let fixture: ComponentFixture<SignboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignboxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SignboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
