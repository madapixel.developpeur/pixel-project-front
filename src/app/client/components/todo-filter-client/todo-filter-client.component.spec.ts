import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoFilterClientComponent } from './todo-filter-client.component';

describe('TodoFilterClientComponent', () => {
  let component: TodoFilterClientComponent;
  let fixture: ComponentFixture<TodoFilterClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoFilterClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoFilterClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
