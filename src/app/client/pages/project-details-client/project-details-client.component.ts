import { Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormlyWrapperFormField } from '@ngx-formly/material/form-field/form-field.wrapper';
import { lastValueFrom } from 'rxjs';
import { ProjectDetailsFieldWrapperComponent } from 'src/app/commons/components/project-details-field-wrapper/project-details-field-wrapper.component';
import { MessageService } from 'src/app/commons/services/message.service';
import { FormData } from 'src/app/model/form-data';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-project-details-client',
  templateUrl: './project-details-client.component.html',
  styleUrls: ['./project-details-client.component.scss']
})
export class ProjectDetailsClientComponent implements OnInit {

  projectStatus: any = environment.projectStatus;
  typesClient: any = environment.typeClient;
  projectId: string|null = null; 
  project: any = null;
  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  form: FormGroup;

  questions : any = null;
  formDatasList : Array<FormData>;
  masterForm: Array<FormGroup>;
  isSaving: boolean = false;

  @ViewChild('saveBtnInput', {static : true}) myTemplateSaveBtn : ElementRef;

  constructor(
    private projectService : ProjectService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private fb: FormBuilder,
    private fileService: FileService) { 
      
      this.formDatasList = [];
      this.form = fb.group({
        details : this.fb.array([]),
      });
      this.masterForm = [];
    }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.paramMap.get("id");
    this.fetchData();
    
  }
  trackByFn(index: number): number {
    return index;
  }
  initMasterForm(){
    console.log(this.project.answers);
    if(this.project.answers!=null){
      let categories : string[] = Object.keys(this.project.answers.categories);
      for(let i=0; i<categories.length; i++){
        let category = categories[i];
        this.masterForm.push(new FormGroup({}));
        const optionsDetails = {parentIndex: i, templateSaveBtn: this.myTemplateSaveBtn};
        let formDatas = this.projectService.getFormDatas(this.project.answers.parent, category, optionsDetails);
        formDatas.MODEL = this.project.answers.categories[category];
        this.formDatasList.push(formDatas);
        console.log(category);
      }
    }
  }
  async fetchData(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getById(this.projectId));
      this.project = result;
      console.log('PROJECT', this.project);
      await this.initMasterForm();
      this.setFormValue(this.project);
      console.log(this.project);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }
  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }

  async getAnswers(){
    let answers : any = {
      parent : this.project.type.questions.parent,
      categories : {}
    };
    for(let i=0; i<this.project.type.questions.categories.length ; i++){
      let formData : FormData = this.formDatasList[i];
      let category : string = this.project.type.questions.categories[i];
      let model : any = formData['MODEL'];
      let modelKeys : string[] = Object.keys(model);
      for(let iField=0; iField<modelKeys.length; iField++){
        let field : string = modelKeys[iField];
        if(model[field] instanceof FileList){
          let results : any[]= await this.uploadFiles(model[field]);
          model[field] = results;
          console.log(field);
          console.log(results);
        }
      }
      answers.categories[category] = model;
    }
    return answers;
  }

  async uploadFiles(fileList : FileList){
    if(!fileList) return[];
    let files = [];
    let results : any[]= [];
      for(let i=0; i<fileList.length; i++){
        const fichier = fileList[i];
        if(fichier){
          files.push({file: fichier});
        } 
      }
      const resFiles: any = await lastValueFrom(this.fileService.uploadFiles(files));
      console.log(resFiles);
      resFiles.forEach((file: any) => {
        results.push({chemin: file.path, nom: file.originalname});
      });
      return results;
  }







  //details
  get details() : FormArray {
    return this.form.get("details") as FormArray;
  }

  async handleOk(){
    this.isSubmitLoading = true;
    try{
      Object.assign(this.project, this.form.value)
      const res: any = await lastValueFrom(this.projectService.updateProjectDetails(this.project))
      this.project = res;
      this.messageService.showSuccess("Projet enregistré avec succès");
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isSubmitLoading = false;
  }

  getFileUrl(filepath: string){
    return this.fileService.getFileUrl(filepath);
  }

  setFormValue(project: any): void{
    
    const detailsFormArray = this.details;
    detailsFormArray.clear();
    this.project.details.forEach((element: any) => {
      detailsFormArray.push(this.new(element));
    }, this);

    detailsFormArray.push(this.new());

  }

  new(details: any = {name: '', description: '', fichiers: []}) : FormGroup {
    const fa: FormArray = this.fb.array([]);
    if(details.fichiers){
      details.fichiers.forEach((fichier: any) => {
        fa.push(this.newFile(fichier))
      }, this);
    }
    
    return this.fb.group({
      name : [details.name, Validators.required],
      description : [details.description, Validators.required],
      fichiers: fa
    });
    
  }

  remove(i : number){
    this.details.removeAt(i);
  }

  add() {
    this.details.push(this.new());
  }

  newFile(file: any) : FormGroup {
    return this.fb.group({
      nom : [file.nom, Validators.required],
      chemin: file.chemin,
      loading: file.loading  
    });
  }

  getFichiers(index: number) : FormArray {
    return this.details.at(index).get("fichiers") as FormArray ;
  }

  removeFichier(i : number, j: number){
    this.getFichiers(i).removeAt(j);
  }

  onFileSelected(index: number, event: any){
    const fichiers: FormArray = this.getFichiers(index);
    const files = event.target.files;
    for(let i=0; i<files.length; i++){
      const fg: FormGroup = this.newFile({
        nom: files[i].name,
        chemin: null,
        loading: true
      });
      fichiers.push(fg);

      const next =  (res: any) => {
        console.log(res);
        fg.get("chemin")?.setValue(res);
      };
      const error = (err: any) => {
        //remove
        const fgIndex = fichiers.controls.findIndex((c: any) => fg == c);
        fichiers.removeAt(fgIndex);
        this.messageService.showError(err);
      };
      const complete = () => fg.get("loading")?.setValue(false);
      this.fileService.uploadFile(files[i]).subscribe({next, error, complete});
    }
    
  }


  async save(savesuccess: CallableFunction | null = null){
    this.isSaving = true;
    try{
      this.project.answers = await this.getAnswers();
      this.project = await lastValueFrom(this.projectService.updateProjectDetails(this.project)) ;
      if(savesuccess) savesuccess();
      this.messageService.showSuccess('Modifications enregistrées');
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    } 
    this.isSaving = false;
    
  }

}

