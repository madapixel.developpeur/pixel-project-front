import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-chat-client',
  templateUrl: './chat-client.component.html',
  styleUrls: ['./chat-client.component.scss']
})
export class ChatClientComponent implements OnInit {

  userIdSelected: string | null = null;
  constructor(private route: ActivatedRoute){}

  ngOnInit(): void{
    this.route.queryParams.subscribe((params: any) => {
      this.userIdSelected = params.f;
    })
  }
  
}
