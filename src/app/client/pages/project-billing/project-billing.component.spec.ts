import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectBillingComponent } from './project-billing.component';

describe('ProjectBillingComponent', () => {
  let component: ProjectBillingComponent;
  let fixture: ComponentFixture<ProjectBillingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectBillingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectBillingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
