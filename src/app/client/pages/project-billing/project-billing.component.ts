import { AfterViewInit, Component, ElementRef, OnInit, ViewChildren } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-project-billing',
  templateUrl: './project-billing.component.html',
  styleUrls: ['./project-billing.component.scss']
})
export class ProjectBillingComponent implements OnInit {

  typesClient: any = environment.typeClient;
  projectId: string|null = null; 
  project: any = null;
  isLoading: boolean = false;
  isSaveLoading: boolean = false;
  
  
  constructor(
    private projectService : ProjectService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private router : Router,
    private fileService: FileService) { 
      
    }

  

  ngOnInit(): void {
    this.projectId = this.route.snapshot.paramMap.get("projectId");
    this.fetchData();
  }


  async fetchData(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getById(this.projectId));
      this.project = result;
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }

  
 async save(data: any){
  this.isSaveLoading = true;
    try{
      const file = await this.fileService.urltoFile(data, "signature.png", "image/png");
      const signaturePath = await lastValueFrom(this.fileService.uploadFile(file));
      await lastValueFrom(this.projectService.billProjectClient({_id: this.projectId, signaturePath}))
      this.messageService.showSuccess("Projet facturé")
      this.router.navigateByUrl("/client/project/"+this.projectId)
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isSaveLoading = false;
 }

 downloadFile(path: string, name: string){
  this.fileService.downloadFile(path, name);
}

getFileUrl(filepath: string){
  return this.fileService.getFileUrl(filepath);
}

}
