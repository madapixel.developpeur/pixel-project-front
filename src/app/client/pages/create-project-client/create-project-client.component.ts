import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import {lastValueFrom} from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { SwiperOptions } from 'swiper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-project-client',
  templateUrl: './create-project-client.component.html',
  styleUrls: ['./create-project-client.component.scss']
})
export class CreateProjectClientComponent implements OnInit {

  isLoading:boolean= false;
  steps: string[] = [
    "step1",
    "step2"
  ];

  forms: FormGroup[] = [];
  currentStep: number = 0;
  project: any = {
    companyOrClient: {}
  }; 

  constructor(
    private projectService: ProjectService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private router: Router) { 
      const formStep1 = fb.group({
        name: [null,[Validators.required]],
        type: [null,[Validators.required]],
        description: [null,[Validators.required]],
      });

      const formStep2 = fb.group({
        companyOrClient: fb.group({
          name: [null,[Validators.required]],
          description: [null,[Validators.required]],
        })
      });

      this.forms = [formStep1, formStep2];
      
  }

  ngOnInit(): void {
  }


  getCurrentForm(): FormGroup {
    return this.forms[this.currentStep];
  }

  async handleOk(){  
    this.isLoading = true;
    try{
      Object.assign(this.project, this.getCurrentForm().value)
      const res: any = {};//await lastValueFrom(this.projectService.createProject(this.steps[this.currentStep], this.project))
      if(!res.companyOrClient) res.companyOrClient = {};
      this.project = res;
      
      if(this.currentStep + 1 < this.steps.length){
        this.currentStep += 1;
      } else{
        this.messageService.showSuccess("Projet ajouté avec succès");
        this.router.navigateByUrl("/client/project/"+this.project._id);
      }
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isLoading = false;
  }
}
