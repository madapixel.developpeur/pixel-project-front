import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectClientComponent } from './create-project-client.component';

describe('CreateProjectClientComponent', () => {
  let component: CreateProjectClientComponent;
  let fixture: ComponentFixture<CreateProjectClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateProjectClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateProjectClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
