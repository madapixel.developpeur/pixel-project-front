import { HttpParams } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@env/environment';
import { lastValueFrom, Subscription } from 'rxjs';
import { GenDatatableComponent } from 'src/app/commons/components/gen-datatable/gen-datatable.component';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { GenTableActionOption } from 'src/app/commons/interfaces/gen-table-action-option';
import { GenTableCustomActionOption } from 'src/app/commons/interfaces/gen-table-custom-action-option';
import { GenTableHeader } from 'src/app/commons/interfaces/gen-table-header';
import { ConfirmService } from 'src/app/commons/services/confirm.service';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-list-client',
  templateUrl: './todo-list-client.component.html',
  styleUrls: ['./todo-list-client.component.scss']
})
export class TodoListClientComponent implements OnInit {

  projectStatus: any = environment.projectStatus;
  projectId: string|null;
  todos: any[] = [];
  isValidateTodosLoading: boolean = false;
  isLoading: boolean = false;
  project: any = null;
  constructor(
    private projectService: ProjectService,
    private todoService: TodoService, 
    private messageService: MessageService, 
    private confirmService: ConfirmService,
    private router : Router,
    private route : ActivatedRoute
  ) {
    this.fetchData = this.fetchData.bind(this)
   }

  headers: GenTableHeader[];


  @ViewChild(GenDatatableComponent) datatable: GenDatatableComponent;
 
  fetchData(options: HttpParams){
    const flattened = flattenObject (this.filter, 'filter'); 
    for(const key in flattened) {
      options = options.set(key, flattened[key]);
    }
    return this.todoService.getTodosClient(this.projectId, options);
  }
  

  filter: any=[];
  async filterResults(filter: any){ 
    this.filter = filter;
    await this.datatable.loadData();
  }
  

   
  async ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get('projectId');
    
   this.headers = [
    {
      title: "Intitulé",
      selector: "name",
      isSortable: true
    },
    {
      title: "Description",
      selector: "description",
      isSortable: true
    },
    {
      title: "Date de création",
      selector: "creationDate",
      type: "date",
      isSortable: true
    },
   
   ];

   this.getProject();
  
  }

  async validateTodos(){
    this.isValidateTodosLoading = true;
    try{
      await lastValueFrom(this.projectService.validateProjectTodosClient(this.projectId))
      this.messageService.showSuccess("Liste à faire validée avec succès")
      this.getProject();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isValidateTodosLoading = false;
  }

  async getProject(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getById(this.projectId));
      this.project = result;
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }


}
