import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListClientComponent } from './todo-list-client.component';

describe('TodoListClientComponent', () => {
  let component: TodoListClientComponent;
  let fixture: ComponentFixture<TodoListClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoListClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoListClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
