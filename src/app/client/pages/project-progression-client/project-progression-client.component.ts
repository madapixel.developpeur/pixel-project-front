import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { TodoService } from 'src/app/services/todo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-project-progression-client',
  templateUrl: './project-progression-client.component.html',
  styleUrls: ['./project-progression-client.component.scss']
})
export class ProjectProgressionClientComponent implements OnInit {

  todoStatus: any = environment.todoStatus;
  isTodoModalVisible: boolean = false;
  todoIdSelected: string | null = null;
  projectId: string|null = null;
  project: any = null;
  isLoading:boolean= false;
  isFinishProjectLoading: boolean = false;
  constructor(
    private todoService: TodoService,
    private projectService: ProjectService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private fileService : FileService,
    private router: Router
    ) {
      this.validateTodo = this.validateTodo.bind(this);
   }




  async getProjectProgression(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getProgressionForClient(this.projectId));
      this.project = result;
      console.log(result);
      //this.initApercu();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  } 

  async ngOnInit() {
    
    this.projectId = this.route.snapshot.paramMap.get("projectId");
    this.route.queryParams.subscribe((params: any) => {
      this.showModal(params.todoId);
    })
    this.getProjectProgression();
  }


  getFileUrl(chemin: string){
    return this.fileService.getFileUrl(chemin);
  }
  
  async finishProject(){
    this.isFinishProjectLoading = true;
    try{
      await lastValueFrom(this.projectService.finishProject(this.projectId))
      this.messageService.showSuccess("Projet terminé avec succès");
      this.router.navigateByUrl("/client/project/"+this.projectId+"/billing")
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isFinishProjectLoading = false;
  }

  showModal(todoId: string | null){
    this.todoIdSelected = todoId;
    this.isTodoModalVisible = true;
  }

  /*
  async initApercu(){
    for(let i=0;i<this.project.todos.todo.length; i++){
      const item : any = this.project.todos.todo[i];
      if(item.apercu!=null && item.apercu.length!=0){
        if(this.project.todos.todo[i].apercuBase64==null) this.project.todos.todo[i].apercuBase64 = [];
        let apercu = item.apercu[0];
        this.project.todos.todo[i].apercuBase64.push(this.fileService.getFileUrl(apercu.chemin));
        //let blob : any = await this.fileService.getBlob(apercu.chemin);
        //let file : File = new File([blob], apercu.nom);
        //this.fileService.toBase64(file).then((url: any) => this.project.todos.todo[i].apercuBase64.push(url));
      }
    }
  }*/

  
  

  async validateTodo(todo: any){
    const index = this.project.todos.finished.findIndex((item: any) => item._id == todo._id);
    let resultTodo = null;
    if(index >= 0){
      resultTodo = this.project.todos.finished[index]; 
      resultTodo.isValidating = true;
      try{
        resultTodo = await lastValueFrom(this.todoService.updateTodoProgressionAdmin({...resultTodo, status: this.todoStatus.validated, progressionDate: new Date()}));
        this.project.todos.finished.splice(index, 1);
        this.project.todos.validated.unshift(resultTodo);
        this.project.todosAllValidated = (this.project.todosLength == this.project.todos.validated.length && this.project.todosLength > 0);
        this.messageService.showSuccess("Tâche validée");
      }catch(e: any){
        console.log(e);
        this.messageService.showError(e)
      } 
      resultTodo.isValidating = false;
    }  
    return resultTodo;
  }

}
