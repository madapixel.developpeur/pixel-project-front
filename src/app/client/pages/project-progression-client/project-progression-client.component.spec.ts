import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectProgressionClientComponent } from './project-progression-client.component';

describe('ProjectProgressionClientComponent', () => {
  let component: ProjectProgressionClientComponent;
  let fixture: ComponentFixture<ProjectProgressionClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectProgressionClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectProgressionClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
