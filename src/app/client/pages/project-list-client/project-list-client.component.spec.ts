import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectListClientComponent } from './project-list-client.component';

describe('ProjectListClientComponent', () => {
  let component: ProjectListClientComponent;
  let fixture: ComponentFixture<ProjectListClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectListClientComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectListClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
