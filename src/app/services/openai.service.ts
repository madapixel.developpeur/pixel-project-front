import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OpenaiService {

  wsUrl: string;

  constructor(private http: HttpClient) { 
    this.wsUrl = environment.wsUrl + "/openai" ;
  }

  generateText(data: any): Observable<any> {
    const url = this.wsUrl+"/text";
    return this.http.post(url,data);
  }

  generateImages(data: any): Observable<any>{
    const url = this.wsUrl+"/images";
    return this.http.post(url,data);
  }
}
