import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormArray } from '@angular/forms';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { lastValueFrom, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as streamSaver from "streamsaver";

@Injectable({
  providedIn: 'root'
})
export class FileService {
  wsUrl: string = environment.wsUrl + "/upload";
  public static defaultAvatarUrl: string = 'assets/app-assets/images/avatars/userimg.png';
  imageExtensions: string[] = ["png", "jpg", "jpeg", "gif", "webp"];

  isImage(fileName: string){
    const fileExt = this.getFileExtension(fileName);
    return this.imageExtensions.indexOf(fileExt) >= 0;
  }

  constructor(private http: HttpClient) { }

  uploadFile(file: File):Observable<any> {
    const formData = new FormData();  
    formData.append("file", file, file.name);
    return this.http.post(this.wsUrl+"/", formData);
  }

  getFileExtension(filename: string){
    // get file extension
    const extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length);
    return extension;
  }

  getFileUrl(filePath: string | null): string | null{
    if(!filePath) return null;
    return environment.wsUrl + "/download?filePath="+filePath;
  }

  getFile(filePath: string, full=false): Observable<any> {
    return this.http.get(full ? filePath : this.getFileUrl(filePath)!, {responseType: 'blob'});
  }
  
  async getBlob(filePath : string, full=false){
    const blob = await lastValueFrom(this.getFile(filePath, full));
    return blob;
  }
  async downloadFile(filePath: string, fileName: string, full=false){
    const blob = await this.getBlob(filePath, full);
    await this.downloadBlob(blob, fileName);
  }

  async downloadFileObject(file: File){
    return await this.downloadBlob(file, file.name);
  }

  async downloadBlob(blob: Blob, fileName: string){
    
    const fileStream = streamSaver.createWriteStream(fileName, {
      size: blob.size 
    })

    const readableStream = blob.stream()
    const w: any = window;

    if (w.WritableStream && readableStream.pipeTo) {
      return readableStream.pipeTo(fileStream)
        .then(() => console.log('done writing'))
    }

    w.writer = fileStream.getWriter()

    const reader = readableStream.getReader()
    const pump = () => reader.read()
      .then((res: any) => res.done
        ? w.writer.close()
        : w.writer.write(res.value).then(pump))

    pump()
  }

  uploadFiles(files: any[]):Observable<any> {
    const formData = new FormData();  
    files.forEach((file: any, index: number) => {
      formData.append("files", file.file, file.name ? file.name : file.file.name);
    }, this)
    return this.http.post(this.wsUrl+"/multiple", formData);
  }

  urltoFile(url: string, filename: string, mimeType: string){
    return (fetch(url)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], filename,{type:mimeType});})
    );
  }

  toBase64(file: any):Promise<String | ArrayBuffer | null> {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

}
