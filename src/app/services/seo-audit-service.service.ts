import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SeoAuditServiceService {
  private apiUrl = 'https://www.seoptimer.com/api/v1/audit';

  constructor(private http: HttpClient) { }

  runAudit(url: string, apiKey: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey
    });
    const body = {
      url: url,
      format: 'json'
    };
    return this.http.post<any>(this.apiUrl, body, { headers: headers });
  }
}
