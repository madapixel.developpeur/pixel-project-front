import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoCollectionUpdate: Subject<any> = new Subject<any>()
  wsUrl: string;
  commentsWsUrl: string;
  constructor(private http: HttpClient) { 
    this.wsUrl = environment.wsUrl + "/todos" ;
    this.commentsWsUrl = environment.wsUrl + "/todo-comments"
  }

  //client
  getTodosClient(projectId: string|null, options: any): Observable<any[]>{
    options = options.set('projectId', projectId);
    const url = this.wsUrl+"/client";
    return this.http.get<any[]>(url, {params: options});
  }

  //admin
  createTodoAdmin(data: any){
    const url = this.wsUrl+"/admin";
    return this.http.post(url,data).pipe(map((res)=>{
      this.todoCollectionUpdate.next(null);
      return res;
    }));
  }

  updateTodoAdmin(data: any){
    const url = this.wsUrl+"/admin";
    return this.http.patch(url,data).pipe(map((res)=>{
      this.todoCollectionUpdate.next(null);
      return res;
    }));
  }

  deleteTodoAdmin(id: string){
    const url = this.wsUrl+"/admin/"+id;
    return this.http.delete(url).pipe(map((res)=>{
      this.todoCollectionUpdate.next(null);
      return res;
    }));
  }

  getTodosAdmin(projectId: string|null, options: any): Observable<any[]>{
    options = options.set('projectId', projectId);
    const url = this.wsUrl+"/admin";
    return this.http.get<any[]>(url, {params: options});
  }
  getTodoByIdAdmin(id: string|null): Observable<any>{
    const url = this.wsUrl+"/admin/"+id;
    return this.http.get<any>(url);
  }
  updateTodoProgressionAdmin(data: any){
    const url = this.wsUrl+"/common/progression";
    return this.http.patch(url,data);
  }
  updateTodoProgressionValueAdmin(data: any){
    const url = this.wsUrl+"/admin/progression-value";
    return this.http.patch(url,data);
  }
  
  //common
  getTodoById(id: string|null): Observable<any>{
    const url = this.wsUrl+"/common/"+id;
    return this.http.get<any>(url);
  }

  createComment(data: any): Observable<any>{
    const url = this.commentsWsUrl+"/common/";
    return this.http.post<any>(url, data);
  }

  getComments(todoId: string|null): Observable<any>{
    const url = this.commentsWsUrl+"/common/"+todoId;
    return this.http.get<any>(url);
  }

}
