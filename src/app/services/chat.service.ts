import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, lastValueFrom, Observable, Subject } from 'rxjs';
import { io, Socket } from "socket.io-client";
import { environment } from 'src/environments/environment';
import { flattenObject } from '../commons/functions/flatten-object';
import { UserService } from './user.service';


@Injectable({
  providedIn: 'root',
})
export class ChatService {

  public static EVENT_NAME = 'chat-message';
  public static SEEN_EVENT_NAME = 'seen-message';
  public message$: BehaviorSubject<any> = new BehaviorSubject(null);
  public seen$: BehaviorSubject<any> = new BehaviorSubject(null);
  wsUrl: string = environment.wsUrl + "/messages";
  socket: Socket;

  public static OPENAI_MESSAGE_CHATBOT = 1;
  public static OPENAI_MESSAGE_GENERATE_TEXT = 2;
  public static OPENAI_MESSAGE_GENERATE_IMAGES = 3;


  public openAIResponse$: Subject<any> = new Subject();
  constructor(private http: HttpClient, private userService: UserService) { 
    this.socket = io(environment.wsUrl)
    this.initOpenAiListener();
  }

  

  public getNewMessage = () => {
    this.socket.on('message', (message: any) =>{
      console.log(message);
      const user = this.userService.getUserDataFromStorage();
      if(user && (user._id == message.userFromId || user._id == message.userToId)) {
        lastValueFrom(this.getMessageById(message.messageId))
        .then((res: any) => {
          if(res){
            this.message$.next(res);
          }
        }).catch((err: any) => {
          console.error(err);
        });
      }
      
    });
    
    return this.message$.asObservable();
  };

  public getSeen = () => {
    this.socket.on('seen', (seen: any) =>{
      console.log(seen);
      const user = this.userService.getUserDataFromStorage();
      if(user && (user._id == seen.userFromId || user._id == seen.userToId )) {
        lastValueFrom(this.getSeenById(seen.seenId))
        .then((res: any) => {
          if(res){
            this.seen$.next(res);
          }
        }).catch((err: any) => {
          console.error(err);
        });
      }
      
    });
    
    return this.seen$.asObservable();
  };



  sendMessage(data: any){
    const url = this.wsUrl+"/send";
    return this.http.post(url,data);
  }

  getChatUsers(params: any){
    const url = this.wsUrl+"/users";
    return this.http.get<any[]>(url, {params: flattenObject(params)});
  }

  getMessages(userId: string | null, params: any){
    const url = this.wsUrl+"/"+userId+"/all";
    return this.http.get<any[]>(url, {params: flattenObject(params)});
  }

  getSeenUser(userId: string | null){
    const url = this.wsUrl+"/seen/"+userId+"/user";
    return this.http.get<any[]>(url);
  }

  getMessageById(messageId: string | null){
    const url = this.wsUrl+"/"+messageId;
    return this.http.get<any[]>(url);
  }

  setSeenNow(userId: string | null){
    const url = this.wsUrl+"/seen/"+userId;
    return this.http.post(url, {});
  }

  getSeenById(seenId: string | null){
    const url = this.wsUrl+"/seen/"+seenId;
    return this.http.get<any[]>(url);
  }

  getMessagesNotSeen(){
    const url = this.wsUrl+"/not-seen";
    return this.http.get<any[]>(url);
  }


  //open AI
  public openAISendMessage = (message: any) => {
    this.socket.emit('messageOpenAI', message);
  };



  initOpenAiListener(){
    this.socket.on('responseOpenAI', (respone: any) =>{
      this.openAIResponse$.next(respone);
    });
  }



}