import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userUpdate = new BehaviorSubject<any>(null);
  wsUrl: string;
  constructor(private http: HttpClient) {
    this.wsUrl = environment.wsUrl;
    this.userUpdate.next(this.getUserDataFromStorage());
   }
   
   getUserData(){
    const url = `${this.wsUrl}/users/user-data`;
    return this.http.get(url);
  }

  getUserInfo(id: string | null){
    const url = `${this.wsUrl}/users/${id}/info`;
    return this.http.get(url);
  }

  logout(){
    const url = `${this.wsUrl}/users/logout`;
    return this.http.get(url).pipe(map((res: any)=>{
      this.removeToken();
      return res;
    }));
  }
  
  login(data: any) {
    const url = `${this.wsUrl}/users/login`
    return this.http.post(url, data).pipe(map((res: any)=>{
      console.log(res);
      this.saveUserData(res.user);
      this.saveToken(res.token);
      return res;
    }));
  }
  signin(data: any){
    const url = `${this.wsUrl}/users/signin`;
    return this.http.post(url,data);
  }
  saveUserData(data : any){
    let userData = data;
    localStorage.setItem('userData', JSON.stringify(userData));
    this.userUpdate.next(data);
  }

  saveToken(token: string){
    localStorage.setItem('token', token);
  }
  getCurrentToken():string|null{
    return localStorage.getItem('token')
  }
  removeToken(){
    localStorage.removeItem('token');
  }
  getNextLink(roleId: number){
    const paths: any = {
      '1': '/client/project',
      '2': '/admin/atelier',
      '3': '/admin/financier'
    }
    return paths[roleId+''];
  }
  checkIfAllowed(roleIds: number[]){
    const url = `${this.wsUrl}/users/can-access`
    return this.http.post(url, {roleIds}).pipe(map((res: any)=>{
      return res.canAccess;
    }));
  }
 
  getUserDataFromStorage(): any{
    let result = localStorage.getItem('userData')
    return result ? JSON.parse(result) : null;
  }

  getClients(options: any): Observable<any[]>{
    const url = this.wsUrl+"/users/clients";
    return this.http.get<any[]>(url, {params: options});
  }

  getClientById(id: string | null): Observable<any>{
    const url = this.wsUrl+"/users/clients/"+id;
    return this.http.get<any[]>(url);
  }

  updateUserInfos(data: any): Observable<any>{
    const url = this.wsUrl+"/users/";
    return this.http.patch<any[]>(url, data).pipe(map((res: any)=>{
      this.saveUserData(res);
      return res;
    }));
  }

  updateUserPassword(data: any): Observable<any>{
    const url = this.wsUrl+"/users/password";
    return this.http.patch<any[]>(url, data);
  }

  
}
