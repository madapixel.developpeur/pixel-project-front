import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import * as QUESTIONS from 'src/assets/Questionnaires';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormData } from '../model/form-data';
import { ProjectDetailsFieldWrapperComponent } from '../commons/components/project-details-field-wrapper/project-details-field-wrapper.component';


@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  wsUrl: string;

  constructor(private http: HttpClient) { 
    this.wsUrl = environment.wsUrl + "/projects" ;
  }
  getFormDatas(type : string, category : string, optionDetails: any = null) : FormData {
    let questions : any = QUESTIONS;
    console.log(type);
    console.log(category);
    
    let tmp : any = questions[type][category];
    
    let formDatas : FormData = {
      TITLE : tmp['TITLE'],
      FORM_CONFIG : tmp['FORM_CONFIG'],
      MODEL : {},
      CATEGORY : category
    }
    
    if(optionDetails){
      formDatas.FORM_CONFIG.forEach((elmt: FormlyFieldConfig, index: number) => {
        elmt.wrappers = [ProjectDetailsFieldWrapperComponent, 'form-field']; 
        elmt.props = {index, myLabel: elmt.templateOptions?.label, parentIndex: optionDetails.parentIndex, templateSaveBtn: optionDetails.templateSaveBtn};
        delete elmt.templateOptions?.label;
      });
    }
    

    return formDatas;
  }
  //client
  getCurrentProject(key: string | null = null){
    let project: any = localStorage.getItem("currentProject");
    project = project ? JSON.parse(project) : {};
    return key ? project[key] : project;
  }
  setCurrentProject(value: any, key: string | null = null){
    let project = value;
    if(key){
      project = this.getCurrentProject();
      project[key] = value;
    }
    localStorage.setItem("currentProject", JSON.stringify(project));
  }
  removeCurrentProject(){
    localStorage.removeItem("currentProject");
  }

  createProject(data: any){
    const url = this.wsUrl+"/create";
    return this.http.post(url,data);
  }

  updateProjectDetails(data: any){
    const url = this.wsUrl+"/update";
    return this.http.post(url,data);
  }

  getById(id: string|null): Observable<any>{
    const url = this.wsUrl+"/client/"+id;
    return this.http.get<any>(url);
  }
  validateProjectTodosClient(id: string|null): Observable<any>{
    const url = this.wsUrl+"/client/"+id+"/validate-todos";
    return this.http.patch<any>(url, {});
  }
  getProjectsForClient(options: any): Observable<any[]>{
    const url = this.wsUrl+"/client";
    return this.http.get<any[]>(url, {params: options});
  }
  getProgressionForClient(id: string|null): Observable<any>{
    const url = this.wsUrl+"/client/"+id+"/progression";
    return this.http.get<any>(url);
  }
  finishProject(id: string|null): Observable<any>{
    const url = this.wsUrl+"/client/"+id+"/finish";
    return this.http.patch<any>(url, {});
  }
  billProjectClient(data: any): Observable<any>{
    const url = this.wsUrl+"/client/bill";
    return this.http.patch<any>(url, data);
  }


  //admin
  getProjectsForAdmin(options: any): Observable<any[]>{
    const url = this.wsUrl+"/admin";
    return this.http.get<any[]>(url, {params: options});
  }
  getByIdForAdmin(id: string|null): Observable<any>{
    const url = this.wsUrl+"/admin/"+id;
    return this.http.get<any>(url);
  }
  validateProjectAdmin(id: string|null): Observable<any>{
    const url = this.wsUrl+"/admin/"+id+"/validate";
    return this.http.patch<any>(url, {});
  }
  getProgressionForAdmin(id: string|null): Observable<any>{
    const url = this.wsUrl+"/admin/"+id+"/progression";
    return this.http.get<any>(url);
  }
  updateDateLivraison(id: string|null, data: any): Observable<any>{
    const url = this.wsUrl+"/admin/"+id+"/livraison";
    return this.http.patch<any>(url, data);
  }

  //common
  getTypes(): Observable<any[]>{
    const url = environment.wsUrl+"/types-project/";
    return this.http.get<any[]>(url);
  }

  
}

