import { TestBed } from '@angular/core/testing';

import { SeoAuditServiceService } from './seo-audit-service.service';

describe('SeoAuditServiceService', () => {
  let service: SeoAuditServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeoAuditServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
