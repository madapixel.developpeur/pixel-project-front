import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountSettingsModule } from 'src/app/account-settings/account-settings.module';
import { AtelierComponent } from './atelier.component';
import { ChatAdminComponent } from './pages/chat-admin/chat-admin.component';
import { ClientDetailsAtelierComponent } from './pages/client-details-atelier/client-details-atelier.component';
import { ClientListAtelierComponent } from './pages/client-list-atelier/client-list-atelier.component';
import { CreateClientComponent } from './pages/create-client/create-client.component';
import { ProjectDetailsAtelierComponent } from './pages/project-details-atelier/project-details-atelier.component';
import { ProjectListAtelierComponent } from './pages/project-list-atelier/project-list-atelier.component';
import { ProjectProgressionAtelierComponent } from './pages/project-progression-atelier/project-progression-atelier.component';
import { TodoListAtelierComponent } from './pages/todo-list-atelier/todo-list-atelier.component';
import { OpenaiModule } from 'src/app/openai/openai.module';


const routes: Routes = [
  {
    path:"",
    component: AtelierComponent,
    children:[
      {
        path: "",
        component: ProjectListAtelierComponent
      },
      {
        path: "project/:id",
        component: ProjectDetailsAtelierComponent
      },
      {
        path: "project/:projectId/todos",
        component: TodoListAtelierComponent
      },
      {
        path: "project/:projectId/progression",
        component: ProjectProgressionAtelierComponent
      },
      {
        path: "chat",
        component: ChatAdminComponent
      },
      {
        path: "create-client",
        component: CreateClientComponent
      },
      {
        path: "clients",
        component: ClientListAtelierComponent
      },
      {
        path: "clients/:id",
        component: ClientDetailsAtelierComponent
      },
      { 
        path : 'account-settings', 
        loadChildren: () => import("src/app/account-settings/account-settings.module").then((m) => AccountSettingsModule) ,
      },
      { 
        path : 'openai', 
        loadChildren: () => import("src/app/openai/openai.module").then((m) => OpenaiModule) ,
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AtelierRoutingModule { }
