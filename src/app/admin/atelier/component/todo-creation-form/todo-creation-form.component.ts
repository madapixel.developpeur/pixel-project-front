import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-creation-form',
  templateUrl: './todo-creation-form.component.html',
  styleUrls: ['./todo-creation-form.component.scss']
})
export class TodoCreationFormComponent implements OnInit{

  form: FormGroup;
  isLoading:boolean= false;
  selectedFiles: File[] = [];
  selectedFilesBase64 : any[] = [];
  
  @Input() isVisible: boolean = false;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  @Input() projectId: string|null ;
  setIsVisible(b: boolean){
    this.isVisible = b;
    this.isVisibleChange.emit(b);
  }
  constructor(
    private fb: FormBuilder, 
    private todoService: TodoService,
    private fileService : FileService,
    private sanitizer : DomSanitizer,
    private messageService: MessageService) { 
      
  }
  
  ngOnInit(): void {
    this.form = this.fb.group({
      name: [null,[Validators.required]],
      description: null,
      apercuFileList: new FormControl(''),
      apercu: null,
      projectId: [this.projectId,[Validators.required]],
    })

  }
  handleCancel(){
    this.setIsVisible(false);
  }
  async handleOk(){
    this.isLoading = true;
    try{
      const values = this.form.value;
      let apercu : any[]= await this.uploadFiles(this.selectedFiles);
      console.log(apercu);
      values.apercu = apercu;
      await lastValueFrom(this.todoService.createTodoAdmin(values))
      this.setIsVisible(false);
      this.messageService.showSuccess("A faire ajouté avec succès")
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isLoading = false;
  }
  async uploadFiles(fileList : File[]){
    if(!fileList) return[];
    let files = [];
    let results : any[]= [];
      for(let i=0; i<fileList.length; i++){
        const fichier = fileList[i];
        console.log(fichier.name);
        if(fichier){
          files.push({file: fichier});
        } 
      }
      const resFiles: any = await lastValueFrom(this.fileService.uploadFiles(files));
      console.log(resFiles);
      resFiles.forEach((file: any) => {
        results.push({chemin: file.path, nom: file.originalname});
      });
      return results;
  }
  onFileInputChange(event: any) {
    this.selectedFiles = Array.from(event.target.files);
  }

  onDelete(index : number) {
    // this.formControl.reset();
    console.log(this.selectedFiles);
    this.selectedFiles.splice(index, 1);
    this.form.get("apercuFileList")?.patchValue(this.selectedFiles);
    console.log("Form Control Value", this.form.get("apercuFileList")?.value);
  }
  getSanitizedImageUrl(file: File) {
    return this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(file)
    );
  }
  isImage(file: File): boolean {
    return /^image\//.test(file.type);
  }

}
