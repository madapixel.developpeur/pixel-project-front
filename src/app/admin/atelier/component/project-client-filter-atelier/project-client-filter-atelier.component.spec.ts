import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectClientFilterAtelierComponent } from './project-client-filter-atelier.component';

describe('ProjectClientFilterAtelierComponent', () => {
  let component: ProjectClientFilterAtelierComponent;
  let fixture: ComponentFixture<ProjectClientFilterAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectClientFilterAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectClientFilterAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
