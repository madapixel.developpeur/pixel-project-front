import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { environment } from '@env/environment';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-project-client-filter-atelier',
  templateUrl: './project-client-filter-atelier.component.html',
  styleUrls: ['./project-client-filter-atelier.component.scss']
})
export class ProjectClientFilterAtelierComponent implements OnInit {
  projectStatus: any = environment.projectStatus;
  @Output() filterClicked = new EventEmitter<any>(true);

  filterValues: any = null;
  types:any[] = [];
  isLoading: boolean = false;

  ngOnInit(){
    this.init();
    this.fetchData();
  }

  generateFilter(){
    const ans: any = [];
    if(this.filterValues.name) {
      ans.push({
        column: 'answers.categories.INFORMATIONS_SUPPLEMENTAIRES.nom',
        comparator: 'like',
        value: this.filterValues.name,
        type: 'string'
      })
    }

    if(this.filterValues.status !== '') {
      ans.push({
        column: 'status',
        comparator: '=',
        value: this.filterValues.status,
        type: 'int'
      })
    }

    if(this.filterValues.dateMin) {
      ans.push({
        column: 'creationDate',
        comparator: '>=',
        value: this.filterValues.dateMin,
        type: 'float'
      })
    }
    if(this.filterValues.dateMax) {
      ans.push({
        column: 'creationDate',
        comparator: '<=',
        value: this.filterValues.dateMax,
        type: 'float'
      })
    }
    if(this.filterValues.type){
      ans.push({
        column: 'type._id',
        comparator: '=',
        value: this.filterValues.type,
        type: 'ObjectId'
      })
    }
    
    return ans;
  }

  constructor(private projectService: ProjectService, private messageService: MessageService) { }

 clickFilter(){
  console.log(this.filterValues)
  this.filterClicked.emit(this.generateFilter());
 }
 

 async fetchData(){
  this.isLoading = true;
  try{
    const res: any = await lastValueFrom(this.projectService.getTypes());
    this.types = res;
  }catch(e: any){
    console.error(e);
    this.messageService.showError(e)
  } 
  this.isLoading = false;
}

 init(){
  this.filterValues = {
    name: '',
    status: '',
    dateMin: '',
    dateMax: '',
    type: ''
  }
  }

}
