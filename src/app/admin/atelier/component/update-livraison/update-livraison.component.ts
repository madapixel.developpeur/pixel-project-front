import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import moment from 'moment';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-update-livraison',
  templateUrl: './update-livraison.component.html',
  styleUrls: ['./update-livraison.component.scss']
})
export class UpdateLivraisonComponent implements OnInit {

  @Input() project: any;
  isSubmitLoading: boolean = false;
  form: FormGroup;
  isLoading:boolean= false;
  isVisible: boolean = false;
  @Input() projectId: string | null;
  @Output() projectChange = new  EventEmitter<any>();
  
  constructor(
    private fb: FormBuilder, 
    private projectService: ProjectService,
    private messageService: MessageService) { 
      this.form = fb.group({
        dateLivraison: [null,[Validators.required]],
      })
  }

  ngOnInit(): void {
    if(this.project){ this.projectId = this.project._id; this.processProject(); }
    else  this.fetchData();
    
  }
  
  async fetchData(){
    this.isLoading = true;
    try{
      this.project = await lastValueFrom(this.projectService.getByIdForAdmin(this.projectId));
      this.processProject();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }
  
  processProject(){
    this.form.setValue({dateLivraison: this.project.dateLivraison?moment(this.project.dateLivraison).format("YYYY-MM-DD") : null});
  }
   
  handleCancel(){
    this.isVisible = false;
  }

  async submit(){
    this.isSubmitLoading = true;
    try{
      const dateLivraison = this.form.value.dateLivraison;
      await lastValueFrom(this.projectService.updateDateLivraison(this.projectId, {dateLivraison}))
      this.project.dateLivraison = dateLivraison;
      this.projectChange.emit(this.project);
      this.handleCancel();
      this.messageService.showSuccess("Date de livraison mise à jour avec succès")
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isSubmitLoading = false;
  }

  showModalUpdateLivraison(){
    this.isVisible = true;
  }
  


  

}
