import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoFilterAtelierComponent } from './todo-filter-atelier.component';

describe('TodoFilterAtelierComponent', () => {
  let component: TodoFilterAtelierComponent;
  let fixture: ComponentFixture<TodoFilterAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoFilterAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoFilterAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
