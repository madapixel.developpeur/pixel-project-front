import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-todo-filter-atelier',
  templateUrl: './todo-filter-atelier.component.html',
  styleUrls: ['./todo-filter-atelier.component.scss']
})
export class TodoFilterAtelierComponent {

  @Output() filterClicked = new EventEmitter<any>(true);

  filterValues: any = {
    name: '',
    description: '',
  }

  generateFilter(){
    const ans: any = [];
    if(this.filterValues.name) {
      ans.push({
        column: 'name',
        comparator: 'like',
        value: this.filterValues.name,
        type: 'string'
      })
    }

    if(this.filterValues.description) {
      ans.push({
        column: 'description',
        comparator: 'like',
        value: this.filterValues.description,
        type: 'string'
      })
    }
    
    return ans;
  }

  constructor() { }

 clickFilter(){
  console.log(this.filterValues)
  this.filterClicked.emit(this.generateFilter());
 }
 

 init(){
  this.filterValues = {
    name: '',
    description: ''
  }
 }

}
