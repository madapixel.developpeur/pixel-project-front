import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-update-form',
  templateUrl: './todo-update-form.component.html',
  styleUrls: ['./todo-update-form.component.scss']
})
export class TodoUpdateFormComponent implements OnInit {

  currentTodo: any;
  isLoadingTodo: boolean = true;
  form: FormGroup;
  isLoading:boolean= false;
  selectedFiles: File[] = [];
  selectedFilesBase64 : any[] = [];

  @Input() isVisible: boolean = false;
  @Input() todoId: string;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  setIsVisible(b: boolean){
    this.isVisible = b;
    this.isVisibleChange.emit(b);
  }
  constructor(
    private fb: FormBuilder, 
    private todoService: TodoService,
    private sanitizer : DomSanitizer,
    private fileService : FileService,
    private messageService: MessageService) { 
  
  }
  async ngOnChanges(changes: SimpleChanges) {
    if(changes['todoId']){
      await this.loadTodo();
    }
  }
  async loadTodo(){
    this.isLoadingTodo = true;
    try{
      this.currentTodo = await lastValueFrom(this.todoService.getTodoByIdAdmin(this.todoId));
      this.form = this.fb.group({
        name: [this.currentTodo.name,[Validators.required]],
        description: this.currentTodo.description,
        apercuFileList: new FormControl(''),
        apercu: null,
        _id: [this.currentTodo._id,[Validators.required]],
      });
      await this.initApercu();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoadingTodo = false;
  }
  async ngOnInit() {
   await this.loadTodo();
  }
   
  handleCancel(){
    this.setIsVisible(false);
  }
  async handleOk(){
    this.isLoading = true;
    try{
      const values = this.form.value;
      let apercu : any[]= await this.uploadFiles(this.selectedFiles);
      console.log(apercu);
      values.apercu = apercu;
      await lastValueFrom(this.todoService.updateTodoAdmin(values))
      this.setIsVisible(false);
      this.messageService.showSuccess("Tâche mise à jour avec succès")
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isLoading = false;
  }
  async initApercu(){
    if(this.currentTodo.apercu==null) return;
    this.selectedFiles = [];
    this.selectedFilesBase64 = [];
    for(let i=0; i<this.currentTodo.apercu.length; i++){
      let filename : string = `Fichier n° ${i}`;
      let blob : any = await this.fileService.getBlob(this.currentTodo.apercu[i].chemin);
      console.log(blob);
      let file : File = new File([blob], filename);
      this.selectedFiles.push(file);
      this.fileService.toBase64(file).then((url: any) => this.selectedFilesBase64.push({
        image : url,
        thumbImage : url
        }));
    }
    this.form.get("apercuFileList")?.patchValue(this.selectedFiles);
    console.log(this.selectedFiles);
    console.log(this.selectedFilesBase64);

  }
  async uploadFiles(fileList : File[]){
    if(!fileList) return[];
    let files = [];
    let results : any[]= [];
      for(let i=0; i<fileList.length; i++){
        const fichier = fileList[i];
        console.log(fichier.name);
        if(fichier){
          files.push({file: fichier});
        } 
      }
      const resFiles: any = await lastValueFrom(this.fileService.uploadFiles(files));
      console.log(resFiles);
      resFiles.forEach((file: any) => {
        results.push({chemin: file.path, nom: file.originalname});
      });
      return results;
  }
  onFileInputChange(event: any) {
    this.selectedFiles = Array.from(event.target.files);
  }

  onDelete(index : number) {
    // this.formControl.reset();
    console.log(this.selectedFiles);
    this.selectedFiles.splice(index, 1);

    this.form.get("apercuFileList")?.patchValue(this.selectedFiles);
    console.log("Form Control Value", this.form.get("apercuFileList")?.value);
  }
  getSanitizedImageUrl(file: File) {
    return this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(file)
    );
  }
  isImage(file: File): boolean {
    return /^image\//.test(file.type);
  }


}
