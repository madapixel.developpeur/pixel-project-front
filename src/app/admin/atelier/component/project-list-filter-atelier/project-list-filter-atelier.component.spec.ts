import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectListFilterAtelierComponent } from './project-list-filter-atelier.component';

describe('ProjectListFilterAtelierComponent', () => {
  let component: ProjectListFilterAtelierComponent;
  let fixture: ComponentFixture<ProjectListFilterAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectListFilterAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectListFilterAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
