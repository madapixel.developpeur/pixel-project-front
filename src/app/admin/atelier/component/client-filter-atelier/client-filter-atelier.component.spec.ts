import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientFilterAtelierComponent } from './client-filter-atelier.component';

describe('ClientFilterAtelierComponent', () => {
  let component: ClientFilterAtelierComponent;
  let fixture: ComponentFixture<ClientFilterAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientFilterAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientFilterAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
