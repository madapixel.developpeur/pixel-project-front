import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-client-filter-atelier',
  templateUrl: './client-filter-atelier.component.html',
  styleUrls: ['./client-filter-atelier.component.scss']
})
export class ClientFilterAtelierComponent implements OnInit{

  @Output() filterClicked = new EventEmitter<any>(true);

  filterValues: any = null;

  ngOnInit(){
    this.init();
  }

  generateFilter(){
    const ans: any = [];
    if(this.filterValues.fullName) {
      ans.push({
        column: 'fullName',
        comparator: 'like',
        value: this.filterValues.fullName,
        type: 'string'
      })
    }

    if(this.filterValues.email) {
      ans.push({
        column: 'email',
        comparator: 'like',
        value: this.filterValues.email,
        type: 'string'
      })
    }
    
    return ans;
  }

  constructor() { }

 clickFilter(){
  console.log(this.filterValues)
  this.filterClicked.emit(this.generateFilter());
 }
 

 init(){
  this.filterValues = {
    fullName: '',
    email: '',
  }
 }

}
