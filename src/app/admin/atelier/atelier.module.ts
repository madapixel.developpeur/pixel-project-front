import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AtelierRoutingModule } from './atelier-routing.module';
import { AtelierComponent } from './atelier.component';
import { HeaderAtelierComponent } from './partials/header-atelier/header-atelier.component';
import { NavAtelierComponent } from './partials/nav-atelier/nav-atelier.component';
import { SharedModule } from 'src/app/commons/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DefaultRepairCreationFormComponent } from './component/default-repair-creation-form/default-repair-creation-form.component';
import { DefaultRepairUpdateFormComponent } from './component/default-repair-update-form/default-repair-update-form.component';
import { DefaultRepairPopupComponent } from './component/default-repair-popup/default-repair-popup.component';
import { ExitFormComponent } from './component/exit-form/exit-form.component';
import { ProjectListAtelierComponent } from './pages/project-list-atelier/project-list-atelier.component';
import { ProjectListFilterAtelierComponent } from './component/project-list-filter-atelier/project-list-filter-atelier.component';
import { ProjectDetailsAtelierComponent } from './pages/project-details-atelier/project-details-atelier.component';
import { TodoFilterAtelierComponent } from './component/todo-filter-atelier/todo-filter-atelier.component';
import { TodoListAtelierComponent } from './pages/todo-list-atelier/todo-list-atelier.component';
import { TodoCreationFormComponent } from './component/todo-creation-form/todo-creation-form.component';
import { TodoUpdateFormComponent } from './component/todo-update-form/todo-update-form.component';
import { ProjectProgressionAtelierComponent } from './pages/project-progression-atelier/project-progression-atelier.component';
import { ChatAdminComponent } from './pages/chat-admin/chat-admin.component';
import { CreateClientComponent } from './pages/create-client/create-client.component';
import { ClientListAtelierComponent } from './pages/client-list-atelier/client-list-atelier.component';
import { ClientFilterAtelierComponent } from './component/client-filter-atelier/client-filter-atelier.component';
import { ProjectClientFilterAtelierComponent } from './component/project-client-filter-atelier/project-client-filter-atelier.component';
import { ClientDetailsAtelierComponent } from './pages/client-details-atelier/client-details-atelier.component';
import { UpdateLivraisonComponent } from './component/update-livraison/update-livraison.component';
 


@NgModule({
  declarations: [
    AtelierComponent,
    HeaderAtelierComponent,
    NavAtelierComponent,
    DefaultRepairCreationFormComponent,
    DefaultRepairUpdateFormComponent,
    DefaultRepairPopupComponent,
    ExitFormComponent,
    ProjectListAtelierComponent,
    ProjectListFilterAtelierComponent,
    ProjectDetailsAtelierComponent,
    TodoFilterAtelierComponent,
    TodoListAtelierComponent,
    TodoCreationFormComponent,
    TodoUpdateFormComponent,
    ProjectProgressionAtelierComponent,
    ChatAdminComponent,
    CreateClientComponent,
    ClientListAtelierComponent,
    ClientFilterAtelierComponent,
    ProjectClientFilterAtelierComponent,
    ClientDetailsAtelierComponent,
    UpdateLivraisonComponent,

  ],
  imports: [
    SharedModule,
    CommonModule,
    AtelierRoutingModule,
    FormsModule,
    DragDropModule,
    ReactiveFormsModule,
  ]
})
export class AtelierModule { }
