import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { MessageService } from 'src/app/commons/services/message.service';
import { lastValueFrom } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-project-welcome',
  templateUrl: './create-project-welcome.component.html',
  styleUrls: ['./create-project-welcome.component.scss']
})
export class CreateProjectWelcomeComponent implements OnInit {


  types: any[] = []; 
  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  form: FormGroup;
  clientId : any = null;
  constructor(
    private route : ActivatedRoute,
    private projectService : ProjectService,
    private messageService: MessageService,
    private router : Router,
    private fb: FormBuilder) { 
      this.form = fb.group({
        type: [null,[Validators.required]],        
      });
    }

  ngOnInit(): void {
    this.clientId = this.route.snapshot.paramMap.get("clientId");
    console.log('clientId', this.clientId);
    const type = this.projectService.getCurrentProject("type");
    if(type) this.form.setValue({type});
    this.fetchData();
  }

  async fetchData(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getTypes());
      this.types = result;
      console.log(this.types);
      this.form.setValue({type: this.types[0]})
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }

  

  async next(){
    try{
      const type = this.form.value.type;     
      this.projectService.setCurrentProject(type, "type");
      this.router.navigateByUrl('/create-project-admin/details/'+this.clientId);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
  }

  async finish(){
    this.isSubmitLoading = true;
    try{
      const type = this.form.value.type;
      const project = {type, userId: this.clientId}
      const res: any = await lastValueFrom(this.projectService.createProject(project)) ;  
      this.projectService.removeCurrentProject();
      this.messageService.showSuccess('Projet créé');
      await this.router.navigateByUrl('/admin/atelier/project/'+res._id);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isSubmitLoading = false;
  }

}
