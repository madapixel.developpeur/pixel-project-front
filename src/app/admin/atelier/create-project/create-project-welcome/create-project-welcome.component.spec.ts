import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProjectWelcomeComponent } from './create-project-welcome.component';

describe('CreateProjectWelcomeComponent', () => {
  let component: CreateProjectWelcomeComponent;
  let fixture: ComponentFixture<CreateProjectWelcomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateProjectWelcomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateProjectWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
