import { CreateProjectDetailsComponent } from './create-project-details/create-project-details.component';
import { CreateProjectWelcomeComponent } from './create-project-welcome/create-project-welcome.component';
import { CreateProjectComponent } from './create-project.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
const routes: Routes = [
  {
    path:"",
    component: CreateProjectComponent,
    children:[
      {
        path: "welcome/:clientId",
        component: CreateProjectWelcomeComponent
      },
      {
        path: "details/:clientId",
        component: CreateProjectDetailsComponent
      },
      {
        path: '', redirectTo: 'welcome', pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateProjectRoutingModule { }
