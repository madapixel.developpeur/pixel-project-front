import { HttpParams } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GenDatatableComponent } from 'src/app/commons/components/gen-datatable/gen-datatable.component';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { GenTableActionOption } from 'src/app/commons/interfaces/gen-table-action-option';
import { GenTableCustomActionOption } from 'src/app/commons/interfaces/gen-table-custom-action-option';
import { GenTableHeader } from 'src/app/commons/interfaces/gen-table-header';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { lastValueFrom } from 'rxjs';


@Component({
  selector: 'app-client-details-atelier',
  templateUrl: './client-details-atelier.component.html',
  styleUrls: ['./client-details-atelier.component.scss']
})
export class ClientDetailsAtelierComponent implements OnInit {
  isLoading: boolean = false;
  clientId: string | null = null;
  projects: any[] = [];
  client: any = null;
  constructor(
    private projectService: ProjectService, 
    private userService: UserService,
    private messageService: MessageService, 
    private router : Router,
    private route: ActivatedRoute
  ) {
    this.fetchData = this.fetchData.bind(this)
   }

  headers: GenTableHeader[];
  actionOptions: GenTableActionOption = {};


  @ViewChild(GenDatatableComponent) datatable: GenDatatableComponent;
 
  @ViewChild("actionColumn", {static: true}) actionColumnTemplate: TemplateRef<any>;

  fetchData(options: HttpParams){
    const filter: any[] = [{
      column: 'userId',
      value: this.clientId,
      comparator: '=',
      type: 'ObjectId'
    }];

    if(this.filter) filter.push(...this.filter)
    
    const flattened = flattenObject (filter, 'filter'); 
    for(const key in flattened) {
      options = options.set(key, flattened[key]);
    }
    console.log(flattened)
    return this.projectService.getProjectsForAdmin(options);
  }
  
  
  filter: any=[];
  async filterResults(filter: any){ 
    this.filter = filter;
    await this.datatable.loadData();
  }
  

  async getClient(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.userService.getClientById(this.clientId));
      this.client = result;
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }
   
  async ngOnInit() {
    this.clientId = this.route.snapshot.paramMap.get("id");

    this.headers = [
      {
        title: "Date de création",
        selector: "creationDate",
        type: "date",
        isSortable: true
      },
      {
        title: "Type de projet",
        selector: "type.nom",
        isSortable: true
      },
      {
        title: "Nom",
        selector: "answers.categories.INFORMATIONS_SUPPLEMENTAIRES.nom",
        isSortable: true
      },
      {
        title: "Description",
        selector: "answers.categories.INFORMATIONS_SUPPLEMENTAIRES.description",
        isSortable: true
      },
      {
        title: "Action",
        selector: "", //Anything goes here it's not important
        template: this.actionColumnTemplate,
        isSortable: false
      }
    ];

    this.getClient();
  
  }

}
