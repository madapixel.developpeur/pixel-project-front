import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDetailsAtelierComponent } from './client-details-atelier.component';

describe('ClientDetailsAtelierComponent', () => {
  let component: ClientDetailsAtelierComponent;
  let fixture: ComponentFixture<ClientDetailsAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientDetailsAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientDetailsAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
