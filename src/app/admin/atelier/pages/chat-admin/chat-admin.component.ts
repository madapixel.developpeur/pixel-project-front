import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-chat-admin',
  templateUrl: './chat-admin.component.html',
  styleUrls: ['./chat-admin.component.scss']
})
export class ChatAdminComponent implements OnInit {

  userIdSelected: string | null = null;
  constructor(private route: ActivatedRoute){}

  ngOnInit(): void{
    this.route.queryParams.subscribe((params: any) => {
      this.userIdSelected = params.f;
      console.log(params);
    })
  }

}
