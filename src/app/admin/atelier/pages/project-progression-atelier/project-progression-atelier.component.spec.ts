import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectProgressionAtelierComponent } from './project-progression-atelier.component';

describe('ProjectProgressionAtelierComponent', () => {
  let component: ProjectProgressionAtelierComponent;
  let fixture: ComponentFixture<ProjectProgressionAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectProgressionAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectProgressionAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
