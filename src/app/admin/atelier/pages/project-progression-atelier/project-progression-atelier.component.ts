import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { TodoService } from 'src/app/services/todo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-project-progression-atelier',
  templateUrl: './project-progression-atelier.component.html',
  styleUrls: ['./project-progression-atelier.component.scss']
})
export class ProjectProgressionAtelierComponent implements OnInit {


  projectId: string|null = null;
  project: any = null;
  isLoading:boolean= false;
  todoStatus: any = environment.todoStatus;
  isTodoModalVisible: boolean = false;
  todoIdSelected: string | null = null;
  apercuBase64: any = {};

  constructor(
    private todoService : TodoService,
    private projectService: ProjectService,
    private messageService: MessageService,
    private fileService : FileService,
    private route : ActivatedRoute,
    private router: Router
    ) {
    
   }


  async getProjectProgression(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getProgressionForAdmin(this.projectId));
      this.project = result;
      //this.initApercu();
      console.log(result);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  } 

  getFileUrl(chemin: string){
    return this.fileService.getFileUrl(chemin);
  }
  
  /*
  async initApercu(){
    for(let i=0;i<this.project.todos.todo.length; i++){
      const item : any = this.project.todos.todo[i];
      if(item.apercu!=null && item.apercu.length!=0){
        if(this.project.todos.todo[i].apercuBase64==null) this.project.todos.todo[i].apercuBase64 = [];
        let apercu = item.apercu[0];
        this.project.todos.todo[i].apercuBase64.push(this.fileService.getFileUrl(apercu.chemin));
        //let blob : any = await this.fileService.getBlob(apercu.chemin);
        //let file : File = new File([blob], apercu.nom);
        //this.fileService.toBase64(file).then((url: any) => this.project.todos.todo[i].apercuBase64.push(url));

      }
    }
  }*/

  async ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get("projectId");
    this.getProjectProgression();
  }

  
  
  async drop(event: CdkDragDrop<string[]>, key: string) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
      
      const data = this.project.todos[key][event.currentIndex];
      const previousStatus = data.status;
      data.status = this.todoStatus[key];
      data.progressionDate = new Date();
      data.isLoading = true;
      try{
        await lastValueFrom(this.todoService.updateTodoProgressionAdmin(data));
        this.calculProjectProgression();
      } catch(error: any){
        console.log(error);
        this.messageService.showError(error);
        transferArrayItem(
          event.container.data,
          event.previousContainer.data,
          event.currentIndex,
          event.previousIndex,
        );
        data.status = previousStatus;
      }
      data.isLoading = false;
    }
    
    
  }


  async updateProgressionValue(item: any){
    item.isValueUpdateLoading = true;
    try{
      await lastValueFrom(this.todoService.updateTodoProgressionValueAdmin(item));
      this.calculProjectProgression();
    } catch(error: any){
      console.log(error);
      this.messageService.showError(error);
    }
    item.isValueUpdateLoading = false;
  }

  calculProjectProgression(){
    let progression = (this.project.todos.finished.length + this.project.todos.validated.length) * 100;
    this.project.todos.inProgress.forEach((element: any) => {
      progression += element.progression;
    });
    progression /= (this.project.todos.todo.length + this.project.todos.inProgress.length + this.project.todos.finished.length + this.project.todos.validated.length);
    this.project.progression = progression;
  }


  showModal(todoId: string | null){
    this.todoIdSelected = todoId;
    this.isTodoModalVisible = true;
  }


  getKey(status: number){
    let result = null;
    Object.keys(this.todoStatus)
    .forEach((key: string) => {
      if(status == this.todoStatus[key]) {
        result = key;
        return;
      }
    });
    return result;
  }

  onTodoUdpated(todo: any){
    const key: string | null = this.getKey(todo.status);
    if(key){
      let index = this.project.todos[key].findIndex((item: any) => todo._id == item._id);
      if(index >= 0) {
        this.project.todos[key].splice(index, 1, todo);
        this.calculProjectProgression();
      }
    }
  }


}
