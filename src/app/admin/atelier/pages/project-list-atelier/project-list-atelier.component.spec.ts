import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectListAtelierComponent } from './project-list-atelier.component';

describe('ProjectListAtelierComponent', () => {
  let component: ProjectListAtelierComponent;
  let fixture: ComponentFixture<ProjectListAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectListAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectListAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
