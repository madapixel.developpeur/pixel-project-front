import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/commons/services/message.service';
import { UserService } from 'src/app/services/user.service';
import { lastValueFrom } from 'rxjs';


@Component({
  selector: 'app-create-client',
  templateUrl: './create-client.component.html',
  styleUrls: ['./create-client.component.scss']
})
export class CreateClientComponent {

  isLoading: boolean = false;
  form: FormGroup;
  constructor(private router: Router, private fb: FormBuilder,private message: MessageService, private userService: UserService) {
    this.form = fb.group({
      'firstName': [null, [Validators.required]],
      'lastName': [null, [Validators.required]],
      'email': [null, [Validators.required]],
      // 'password': [null, [Validators.required]],
      // 'confirmPassword': [null, [Validators.required]],
      
    });
   }
   
   async submit(){
    this.isLoading = true;
    try{
      const data = this.form.value;
      const res: any = await lastValueFrom(this.userService.signin(data));
      await this.router.navigateByUrl('/create-project-admin/welcome/'+res.userId);
      this.message.showSuccess('Client enregistré avec succès');
    }catch(e: any){
      console.log(e);
      this.message.showError(e)
    }
    this.isLoading = false;
   }

}
