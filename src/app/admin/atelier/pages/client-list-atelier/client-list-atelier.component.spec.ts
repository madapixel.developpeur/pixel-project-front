import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListAtelierComponent } from './client-list-atelier.component';

describe('ClientListAtelierComponent', () => {
  let component: ClientListAtelierComponent;
  let fixture: ComponentFixture<ClientListAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClientListAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ClientListAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
