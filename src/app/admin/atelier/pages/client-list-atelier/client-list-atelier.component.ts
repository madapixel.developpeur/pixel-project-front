import { HttpParams } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { GenDatatableComponent } from 'src/app/commons/components/gen-datatable/gen-datatable.component';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { GenTableActionOption } from 'src/app/commons/interfaces/gen-table-action-option';
import { GenTableCustomActionOption } from 'src/app/commons/interfaces/gen-table-custom-action-option';
import { GenTableHeader } from 'src/app/commons/interfaces/gen-table-header';
import { MessageService } from 'src/app/commons/services/message.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-client-list-atelier',
  templateUrl: './client-list-atelier.component.html',
  styleUrls: ['./client-list-atelier.component.scss']
})
export class ClientListAtelierComponent implements OnInit {

  clients: any[] = [];
  constructor(
    private userService: UserService, 
    private messageService: MessageService, 
    private router : Router
  ) {
    this.fetchData = this.fetchData.bind(this)
   }

  headers: GenTableHeader[];
  actionOptions: GenTableActionOption = {};


  @ViewChild(GenDatatableComponent) datatable: GenDatatableComponent;
 
  @ViewChild("actionColumn", {static: true}) actionColumnTemplate: TemplateRef<any>;

  fetchData(options: HttpParams){
    const flattened = flattenObject (this.filter, 'filter'); 
    for(const key in flattened) {
      options = options.set(key, flattened[key]);
    }
    console.log(flattened)
    return this.userService.getClients(options);
  }
  
  
  filter: any=[];
  async filterResults(filter: any){ 
    this.filter = filter;
    await this.datatable.loadData();
  }
  

   
  async ngOnInit() {
    
    this.headers = [
      {
        title: "Nom et prénom(s)",
        selector: "fullName",
        isSortable: true
      },
      {
        title: "Adresse email",
        selector: "email",
        isSortable: true
      },
      {
        title: "Action",
        selector: "", //Anything goes here it's not important
        template: this.actionColumnTemplate,
        isSortable: false
      }
    ];
  
  }

}
