import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FormData } from 'src/app/model/form-data';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-project-details-atelier',
  templateUrl: './project-details-atelier.component.html',
  styleUrls: ['./project-details-atelier.component.scss']
})
export class ProjectDetailsAtelierComponent implements OnInit {
  isUpdateLivraisonModalVisible: boolean = false;
  projectStatus: any = environment.projectStatus;
  typesClient: any = environment.typeClient;
  projectId: string|null = null; 
  project: any = null;
  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  form: FormGroup;

  questions : any = null;
  formDatasList : Array<FormData>;
  isValidateProjectLoading : boolean = false;

  constructor(
    private projectService : ProjectService,
    private messageService: MessageService,
    private route : ActivatedRoute,
    private fb: FormBuilder,
    private fileService: FileService) { 
      this.formDatasList = [];
      this.form = fb.group({
        details : this.fb.array([]),
      });
    }

  ngOnInit(): void {
    this.projectId = this.route.snapshot.paramMap.get("id");
    this.fetchData();
    
  }
  trackByFn(index: number): number {
    return index;
  }
  initMasterForm(){
    console.log(this.project.answers);
    if(this.project.answers!=null){
      let categories : string[] = Object.keys(this.project.answers.categories);
      for(let i=0; i<categories.length; i++){
        let category = categories[i];
        let formDatas = this.projectService.getFormDatas(this.project.answers.parent, category);
        this.formDatasList.push(formDatas);
        console.log(category);
      }
    }
  }
  get details() : FormArray {
    return this.form.get("details") as FormArray;
  }

  async fetchData(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getByIdForAdmin(this.projectId));
      this.project = result;
      await this.initMasterForm();
      console.log(this.project);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }


  async validateProject(){
    this.isValidateProjectLoading = true;
    try{
      await lastValueFrom(this.projectService.validateProjectAdmin(this.projectId))
      this.messageService.showSuccess("Projet validé avec succès")
      this.fetchData();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isValidateProjectLoading = false;
  }

  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }

  getFileUrl(filepath: string){
    return this.fileService.getFileUrl(filepath);
  }

  showModalUpdateLivraison(){
    this.isUpdateLivraisonModalVisible = true;
  }

  
  
}
