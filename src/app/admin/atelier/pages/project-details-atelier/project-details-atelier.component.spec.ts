import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDetailsAtelierComponent } from './project-details-atelier.component';

describe('ProjectDetailsAtelierComponent', () => {
  let component: ProjectDetailsAtelierComponent;
  let fixture: ComponentFixture<ProjectDetailsAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectDetailsAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectDetailsAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
