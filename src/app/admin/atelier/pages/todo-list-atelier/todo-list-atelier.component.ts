import { HttpParams } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Subscription } from 'rxjs';
import { GenDatatableComponent } from 'src/app/commons/components/gen-datatable/gen-datatable.component';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { GenTableActionOption } from 'src/app/commons/interfaces/gen-table-action-option';
import { GenTableCustomActionOption } from 'src/app/commons/interfaces/gen-table-custom-action-option';
import { GenTableHeader } from 'src/app/commons/interfaces/gen-table-header';
import { ConfirmService } from 'src/app/commons/services/confirm.service';
import { MessageService } from 'src/app/commons/services/message.service';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo-list-atelier',
  templateUrl: './todo-list-atelier.component.html',
  styleUrls: ['./todo-list-atelier.component.scss']
})
export class TodoListAtelierComponent implements OnInit {

  projectId: string|null;
  todos: any[] = [];
  isCreationModalVisible: boolean = false;
  todosUpdateSub: Subscription;
  constructor(
    private todoService: TodoService, 
    private messageService: MessageService, 
    private confirmService: ConfirmService,
    private router : Router,
    private route : ActivatedRoute
  ) {
    this.fetchData = this.fetchData.bind(this)
    this.updateTodo = this.updateTodo.bind(this)
    this.deleteTodo = this.deleteTodo.bind(this)
   }

  headers: GenTableHeader[];
  actionOptions: GenTableActionOption = {};


  @ViewChild(GenDatatableComponent) datatable: GenDatatableComponent;
 
  @ViewChild("actionColumn", {static: true}) actionColumnTemplate: TemplateRef<any>;

  fetchData(options: HttpParams){
    const flattened = flattenObject (this.filter, 'filter'); 
    for(const key in flattened) {
      options = options.set(key, flattened[key]);
    }
    return this.todoService.getTodosAdmin(this.projectId, options);
  }
  
  openModal(){
    this.isCreationModalVisible = true;
  }

  filter: any=[];
  async filterResults(filter: any){ 
    this.filter = filter;
    await this.datatable.loadData();
  }
  

   
  async ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get('projectId');
    this.todosUpdateSub = this.todoService.todoCollectionUpdate.subscribe(async ()=>{
      this.datatable.loadData();
    })
   this.headers = [
    {
      title: "Intitulé",
      selector: "name",
      isSortable: true
    },
    {
      title: "Description",
      selector: "description",
      isSortable: true
    },
    {
      title: "Date de création",
      selector: "creationDate",
      type: "date",
      isSortable: true
    },
    {
      title: "Action",
      selector: "description", //Anything goes here it's not important
      template: this.actionColumnTemplate,
      isSortable: false
    },
   
   ];
  
  }
  ngOnDestroy(): void {
    this.todosUpdateSub.unsubscribe();
  }

  currentUpdateTodoId: string;
  isUpdateModalVisible: boolean = false;
  updateTodo(row: any){
    this.currentUpdateTodoId = row._id;
    this.isUpdateModalVisible = true;
  }

  deleteTodo(row: any){
    this.confirmService.showConfirm('Supprimer cet élément?', async ()=>{
      await lastValueFrom(this.todoService.deleteTodoAdmin(row._id));
      this.messageService.showSuccess('A faire retiré');
    })
  }

  getActionOptions(row: any){
    const actionOptions: GenTableCustomActionOption[] = [
      {
        label: 'Modifier',
        actionFunction: this.updateTodo
      }, 
      {
        label: 'Supprimer',
        actionFunction: this.deleteTodo
      }, 
    ];
    return actionOptions;
  }

}
