import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListAtelierComponent } from './todo-list-atelier.component';

describe('TodoListAtelierComponent', () => {
  let component: TodoListAtelierComponent;
  let fixture: ComponentFixture<TodoListAtelierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoListAtelierComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TodoListAtelierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
