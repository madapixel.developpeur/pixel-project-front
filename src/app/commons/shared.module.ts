import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './partials/footer/footer.component';
import { GenModalComponent } from './components/gen-modal/gen-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenButtonComponent } from './components/gen-button/gen-button.component';
import { GenMessageComponent } from './components/gen-message/gen-message.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GenDatatableComponent } from './components/gen-datatable/gen-datatable.component';
import { AccessDeniedComponent } from './pages/access-denied/access-denied.component';
import { GenConfirmComponent } from './components/gen-confirm/gen-confirm.component';
import { CarStatusComponent } from './components/car-status/car-status.component';
import { CarFilterComponent } from './components/car-filter/car-filter.component';
import { DefaultRepairFilterComponent } from './components/default-repair-filter/default-repair-filter.component';
import { AccountingStatsDatatableComponent } from './components/accounting-stats-datatable/accounting-stats-datatable.component';
import { HistoricFilterComponent } from './components/historic-filter/historic-filter.component';
import { RepairPaymentStatusComponent } from './components/repair-payment-status/repair-payment-status.component';
import { MyFileSelectComponent } from './components/my-file-select/my-file-select.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatUserComponent } from './components/chat-user/chat-user.component';
import { MyCreateProjectDetailsComponent } from './components/my-create-project-details/my-create-project-details.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { SwiperModule } from 'swiper/angular';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyMatTextAreaModule } from '@ngx-formly/material/textarea';
import { FormlyMatDatepickerModule } from '@ngx-formly/material/datepicker';
import { FormlyMatSliderModule } from '@ngx-formly/material/slider';
import { FormlyMatSelectModule } from '@ngx-formly/material/select';
import { FormlyMatMultiCheckboxModule } from '@ngx-formly/material/multicheckbox';
import { FormlyMatRadioModule } from '@ngx-formly/material/radio';
import { FormlyMatNativeSelectModule } from '@ngx-formly/material/native-select';
import { FormlyMatFormFieldModule } from '@ngx-formly/material/form-field';
import { FormlyMatCheckboxModule } from '@ngx-formly/material/checkbox';
import { FormlyMatInputModule } from '@ngx-formly/material/input';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { FormlyFieldFileComponent } from '../components/formly-field-file/formly-field-file.component';
import { FormlyModule } from '@ngx-formly/core';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatLegacyFormFieldModule as MatFormFieldModule } from '@angular/material/legacy-form-field';
import { MatLegacyInputModule as MatInputModule } from '@angular/material/legacy-input';
import { FormlyFieldFileDirective } from '../components/formly-field-file/formly-field-file.directive';
import { TodoModalComponent } from './components/todo-modal/todo-modal.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { NgImageSliderModule } from 'ng-image-slider';
import { ProjectDetailsFieldWrapperComponent } from './components/project-details-field-wrapper/project-details-field-wrapper.component';
import { GenerateTextComponent } from './components/generate-text/generate-text.component';
import { TextareaAutoresizeDirective } from './components/textarea-autoresize.directive';
import { GenerateImagesComponent } from './components/generate-images/generate-images.component';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    FooterComponent,
    GenModalComponent,
    GenButtonComponent,
    GenMessageComponent,
    GenDatatableComponent,
    AccessDeniedComponent,
    GenConfirmComponent,
    CarStatusComponent,
    CarFilterComponent,
    DefaultRepairFilterComponent,
    AccountingStatsDatatableComponent,
    HistoricFilterComponent,
    RepairPaymentStatusComponent,
    MyFileSelectComponent,
    ChatComponent,
    ChatUserComponent,
    FormlyFieldFileDirective, 
    FormlyFieldFileComponent,
    MyCreateProjectDetailsComponent,
    TodoModalComponent,
    ProjectDetailsFieldWrapperComponent,
    GenerateTextComponent,
    TextareaAutoresizeDirective,
    GenerateImagesComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    NgbModule,
    PerfectScrollbarModule,
    SwiperModule,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      types: [{ name: 'file', component: FormlyFieldFileComponent, wrappers: ['form-field'] }],
    }),
    FormlyBootstrapModule,
    FormlyMatInputModule,
    FormlyMatCheckboxModule,
    FormlyMatFormFieldModule,
    FormlyMatNativeSelectModule,
    FormlyMatRadioModule,
    FormlyMatMultiCheckboxModule,
    FormlyMatSelectModule,
    FormlyMatSliderModule,
    FormlyMatDatepickerModule,
    FormlyMatTextAreaModule,
    FormlyMaterialModule, // Do not forget to import this !!
    NgxImageZoomModule,
    NgImageSliderModule
  ],
  exports:[
    FooterComponent,
    GenModalComponent,
    GenButtonComponent,
    GenMessageComponent,
    GenDatatableComponent,
    AccessDeniedComponent,
    GenConfirmComponent,
    CarStatusComponent,
    CarFilterComponent,
    DefaultRepairFilterComponent,
    HistoricFilterComponent,
    AccountingStatsDatatableComponent,
    RepairPaymentStatusComponent,
    MyFileSelectComponent,
    ChatComponent,
    MyCreateProjectDetailsComponent,
    TodoModalComponent,
    ProjectDetailsFieldWrapperComponent,
    GenerateTextComponent,
    GenerateImagesComponent
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    },
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
  ],
})
export class SharedModule { }
