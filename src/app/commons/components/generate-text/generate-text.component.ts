import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/commons/services/message.service';
import { lastValueFrom } from 'rxjs';
import { OpenaiService } from 'src/app/services/openai.service';

@Component({
  selector: 'app-generate-text',
  templateUrl: './generate-text.component.html',
  styleUrls: ['./generate-text.component.scss']
})
export class GenerateTextComponent implements OnInit{
  isLoading: boolean = false;
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private message: MessageService, 
    private openaiService: OpenaiService) {
    this.form = fb.group({
      'req': [null, [Validators.required]],
      'resp': null,
    });
   }
  ngOnInit(): void {
    
  }
   
  async submit(){
    this.isLoading = true;
    try{
      const data = {req: this.form.controls.req.value};
      const resp = await lastValueFrom(this.openaiService.generateText(data));
      this.form.controls.resp.setValue(resp);
    } catch(error){
      this.message.showError(error);
    } 
    this.isLoading = false;
   }
}
