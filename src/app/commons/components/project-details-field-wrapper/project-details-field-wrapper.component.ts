import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-project-details-field-wrapper',
  templateUrl: './project-details-field-wrapper.component.html',
  styleUrls: ['./project-details-field-wrapper.component.scss']
})
export class ProjectDetailsFieldWrapperComponent extends FieldWrapper implements OnInit {
  
  fieldValue: any = null;
  showInput: boolean = false;

  constructor(private fileService: FileService){
    super();
    this.hideInput = this.hideInput.bind(this);
  }

  ngOnInit(): void {
    
    this.fieldValue = this.formControl.value;
    this.formControl.valueChanges.subscribe({
      next: (value: any) => {
        this.fieldValue = value;
      }
    })
  }
  
  edit(){
    this.showInput = true;
  }

  hideInput(){
    this.showInput = false;
  }

  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }
}