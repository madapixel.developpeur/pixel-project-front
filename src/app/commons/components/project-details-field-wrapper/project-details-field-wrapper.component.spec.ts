import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDetailsFieldWrapperComponent } from './project-details-field-wrapper.component';

describe('ProjectDetailsFieldWrapperComponent', () => {
  let component: ProjectDetailsFieldWrapperComponent;
  let fixture: ComponentFixture<ProjectDetailsFieldWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectDetailsFieldWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectDetailsFieldWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
