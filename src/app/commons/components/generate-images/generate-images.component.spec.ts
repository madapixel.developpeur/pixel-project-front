import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateImagesComponent } from './generate-images.component';

describe('GenerateImagesComponent', () => {
  let component: GenerateImagesComponent;
  let fixture: ComponentFixture<GenerateImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerateImagesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GenerateImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
