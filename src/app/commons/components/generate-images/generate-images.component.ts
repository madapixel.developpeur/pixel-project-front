import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'src/app/commons/services/message.service';
import { lastValueFrom } from 'rxjs';
import { OpenaiService } from 'src/app/services/openai.service';
import { FileService } from 'src/app/services/file.service';
import moment from 'moment';

@Component({
  selector: 'app-generate-images',
  templateUrl: './generate-images.component.html',
  styleUrls: ['./generate-images.component.scss']
})
export class GenerateImagesComponent {
  isLoading: boolean = false;
  form: FormGroup;
  results: any[] = [/* {
    url: 'https://oaidalleapiprodscus.blob.core.windows.net/private/org-vCXFBEt4LIJ3LVQSjpGYkSIK/user-OEVimcVauQvvIY11EPrRORHE/img-464GmC4yl7Prwssrx6WvqkYg.png?st=2023-04-19T13%3A40%3A23Z&se=2023-04-19T15%3A40%3A23Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-04-19T13%3A10%3A04Z&ske=2023-04-20T13%3A10%3A04Z&sks=b&skv=2021-08-06&sig=QXR0Hy4oISPEgmHyfsEa2qjvMC%2BBitVSXLYupD1Ktmo%3D'
  }, {
    url: 'https://oaidalleapiprodscus.blob.core.windows.net/private/org-vCXFBEt4LIJ3LVQSjpGYkSIK/user-OEVimcVauQvvIY11EPrRORHE/img-qIIwx8050oh6cPkbN2zG6yDY.png?st=2023-04-19T13%3A40%3A23Z&se=2023-04-19T15%3A40%3A23Z&sp=r&sv=2021-08-06&sr=b&rscd=inline&rsct=image/png&skoid=6aaadede-4fb3-4698-a8f6-684d7786b067&sktid=a48cca56-e6da-484e-a814-9c849652bcb3&skt=2023-04-19T13%3A10%3A04Z&ske=2023-04-20T13%3A10%3A04Z&sks=b&skv=2021-08-06&sig=MxEmxngWdnsFVzjE%2BbbMflOzHji30Clol2vE1J3TDA4%3D'
  } */] 
  constructor(
    private fb: FormBuilder,
    private message: MessageService, 
    private openaiService: OpenaiService,
    private fileService: FileService) {
    this.form = fb.group({
      'prompt': [null, [Validators.required]],
      'n': [1, [Validators.required]],
      'size': ["512x512", [Validators.required]],
    });
   }

  ngOnInit(): void {
    
  }
   
  async submit(){
    this.isLoading = true;
    try{
      this.results = await lastValueFrom(this.openaiService.generateImages(this.form.value));
    } catch(error){
      this.message.showError(error);
    } 
    this.isLoading = false;
   }

  
}
