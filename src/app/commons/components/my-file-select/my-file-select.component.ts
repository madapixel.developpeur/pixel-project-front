import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-my-file-select',
  templateUrl: './my-file-select.component.html',
  styleUrls: ['./my-file-select.component.scss']
})
export class MyFileSelectComponent {

  @Input() requiredFileType: string;
  @Output() fileSelected = new EventEmitter<any>(true);
  @Input() label = '';

  onFileSelected(event: any) {
    const file: File = event.target.files[0];

    if (file) {
      this.fileSelected.emit(file);
    }
  }


}
