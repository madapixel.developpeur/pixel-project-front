import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFileSelectComponent } from './my-file-select.component';

describe('MyFileSelectComponent', () => {
  let component: MyFileSelectComponent;
  let fixture: ComponentFixture<MyFileSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyFileSelectComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyFileSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
