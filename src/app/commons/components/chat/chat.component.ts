import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import {lastValueFrom} from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { SwiperOptions } from 'swiper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import moment from 'moment';
import { FileService } from 'src/app/services/file.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnChanges {

  page: number = 1;
  pageElmtCount: number = 10;
  isMoreMessageLoading: boolean = false;
  thereAreMoreMessages: boolean = false;

  chatUsers: any[] = [];
  isLoading:boolean= false;
  search: string | null = null; 
  me: any = null;
  @Input() userIdSelected: string | null = null;

  constructor(
    private userService: UserService,
    private chatService: ChatService,
    private messageService: MessageService,
    private fb: FormBuilder,
    private router: Router,
    private fileService: FileService) { 
      
  }

  ngOnInit(): void {
    this.me = this.userService.getUserDataFromStorage();
    window.addEventListener(ChatService.EVENT_NAME, (event: any) => {
      this.addNewMessage(event.detail);
    });
    window.addEventListener(ChatService.SEEN_EVENT_NAME, (event: any) => {
      this.setSeenChatUser(event.detail);
    });
    this.init();
  }

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['userIdSelected'];
    if(change.firstChange || change.currentValue != change.previousValue){
      
    }
  }

  init(){
    this.getChatUsersFirst();
  }

  addNewMessage(message: any){
    const index = this.chatUsers.findIndex((elmt: any) => elmt._id == message.userFromId || elmt._id == message.userToId);
    if(index >= 0){
      const chatUser = this.chatUsers[index];
      chatUser.message = message;
      this.chatUsers.splice(index, 1);
      this.chatUsers.unshift(chatUser);
    }
  }

  setSeenChatUser(seen: any){
    const index = this.chatUsers.findIndex((elmt: any) => elmt._id == seen.userToId);
    if(index >= 0){
      const chatUser = this.chatUsers[index];
      chatUser.ms = seen;
    }
  }


  isActiveChat(userId: string){
    return userId == this.userIdSelected;
  }

  setActiveChat(userId: string){
    this.userIdSelected = userId;
  }

  async getChatUsers(page: number){
    try{
      const options = {search: this.search, pagination: {page, pageElmtCount: this.pageElmtCount}};
      const res: any = await lastValueFrom(this.chatService.getChatUsers(options))
      this.chatUsers.push(...res.data);
      this.thereAreMoreMessages = this.chatUsers.length == (page * this.pageElmtCount);
      this.page = page;
    }catch(e: any){
      console.error(e);
      this.messageService.showError(e)
    } 
  }

  searchOnEnter(e: any){
    if (e.key === 'Enter' || e.keyCode === 13) {
      this.getChatUsersFirst();
    }
  }

  async getChatUsersFirst(){
    this.chatUsers = [];
    this.isLoading = true;
    await this.getChatUsers(1);
    this.isLoading = false;
  }

  async seeMoreChatUsers(){
    this.isMoreMessageLoading = true;
    await this.getChatUsers(this.page + 1);
    this.isMoreMessageLoading = false;
  }

  messageIsFromMe(chatUser: any){
    return chatUser.message.userFromId != chatUser._id
  }

  isNew(chatUser: any): boolean{
    
    if(this.isActiveChat(chatUser._id)) return false;
    if(this.messageIsFromMe(chatUser)) return false;
    if(chatUser.ms && chatUser.ms.lastSeenDate) {
      return !moment(chatUser.ms.lastSeenDate).isAfter(moment(chatUser.message.sentDate));
    } 
    return true;
  }

  getAvatarUrl(chemin: string | null){
    return  chemin ? this.fileService.getFileUrl(chemin) : FileService.defaultAvatarUrl;
  }
}
