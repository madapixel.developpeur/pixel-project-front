import { PerfectScrollbarDirective } from 'ngx-perfect-scrollbar';
import { Component, OnInit, ViewChild, AfterViewInit, Input, SimpleChanges, OnChanges, ChangeDetectorRef } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import {lastValueFrom} from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { SwiperOptions } from 'swiper';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { flattenObject } from 'src/app/commons/functions/flatten-object';
import { UserService } from 'src/app/services/user.service';
import  * as moment from 'moment';
import { FileService } from 'src/app/services/file.service';

@Component({
  selector: 'app-chat-user',
  templateUrl: './chat-user.component.html',
  styleUrls: ['./chat-user.component.scss']
})
export class ChatUserComponent implements OnInit, AfterViewInit, OnChanges {

  fileBoxHeight: number = 50;
  spaceHeight: number = 0;

  page: number = 1;
  pageElmtCount: number = 10;

  messages: any[] = [];
  messagesSplited: any[] = [];
  isLoading:boolean= false;
  isSendingMessage: boolean = false;
  isMoreMessageLoading: boolean = false;
  thereAreMoreMessages: boolean = false;
  isLoadingUser: boolean = false;
  form: FormGroup; 
  search: string | null = null;
  me: any = null;
  fichiers: any[] = [];
  seen: any = null;
  
  @Input() userId: string | null;
  user: any = null;
  @ViewChild('userChats') userChatsRef?: PerfectScrollbarDirective;
  

  constructor(
    private chatService: ChatService,
    private messageService: MessageService,
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private fileService: FileService,
    private ref: ChangeDetectorRef) { 
      this.form = fb.group({
        contenu: null,
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['userId'];
    if(change.firstChange || change.currentValue != change.previousValue){
      this.init();
    }
  }
  
  ngOnInit(): void {
    this.me = this.userService.getUserDataFromStorage();
    window.addEventListener(ChatService.EVENT_NAME, (event: any) => {
      this.addNewMessage(event.detail);
    });
    window.addEventListener(ChatService.SEEN_EVENT_NAME, (event: any) => {
      this.setSeenChatUser(event.detail);
    });
    
  } 

  ngAfterViewInit(): void{
  }

  setSeenChatUser(seen: any){
    if(this.userId == seen.userFromId){
      this.seen = seen;
    } 
  }
  
  addNewMessage(message: any): void {
    if(this.userId == message.userFromId || this.userId == message.userToId){
      this.pushMessage(message);
      if(this.userId == message.userFromId){
        this.setDateSeenNow();
      }
      
    }
  }

  async setDateSeenNow(){
    try{
      const res = await lastValueFrom(this.chatService.setSeenNow(this.userId));
      console.log(res);
    } catch(err: any){
      console.error(err);
    }
  }


  init(): void {
    this.resetForm();
    this.getUserInfo();
    this.getMessagesFirst();
    this.getSeen();
  }

  scrollToBottom(){
    this.ref.detectChanges();
    this.userChatsRef?.update();
    this.userChatsRef?.scrollToBottom(0, 400);
  }

  resetForm(){
    this.fichiers = [];
    this.form.setValue({contenu: null});
  }

  removeFichier(i : number){
    this.fichiers.splice(i, 1);
  }

  onFileSelected(event: any){
    const files = event.target.files;
    for(let i=0; i<files.length; i++){
      const nf = {
        nom: files[i].name,
        chemin: null,
        loading: true
      };
      this.fichiers.push(nf);

      const next =  (res: any) => {
        nf.chemin = res;
      };

      const error = (err: any) => {
        //remove
        const nfIndex = this.fichiers.findIndex((f: any) => f == nf);
        this.fichiers.splice(nfIndex);
        this.messageService.showError(err);
      };

      const complete = () => nf.loading = false;

      this.fileService.uploadFile(files[i]).subscribe({next, error, complete});
    }
    
  }

  async sendMessage(){
    if(this.isSendingMessage) return;
    this.isSendingMessage = true;
    try{
      const data = {...this.form.value, fichiers: this.fichiers};
      data.userToId = this.userId;
      console.log(data);
      const res: any = await lastValueFrom(this.chatService.sendMessage(data))
      this.resetForm();
      /*
      this.pushMessage(res);
      window.dispatchEvent(new CustomEvent('chat-new-message', {
        detail: res
      }));*/
      this.messageService.showSuccess("Message envoyé");
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isSendingMessage = false;
  }

  pushMessage(message: any){
    this.messages.push(message);
    this.splitMessages();
    this.scrollToBottom();
  }

  async getMessages(page: number, onSuccess: CallableFunction | null = null){
    try{
      const options = {search: this.search, pagination: {page, pageElmtCount: this.pageElmtCount}};
      const res: any = await lastValueFrom(this.chatService.getMessages(this.userId, options))
      this.messages.unshift(...res.data.reverse());
      console.log(res);
      this.thereAreMoreMessages = this.messages.length < res.meta.totalElmtCount;
      this.page = page;
      this.splitMessages();
      if(onSuccess)  onSuccess();
    }catch(e: any){
      console.error(e);
      this.messageService.showError(e)
    } 
  }

  async getSeen(){
    try{
      const res: any = await lastValueFrom(this.chatService.getSeenUser(this.userId))
      console.log(res);
      this.seen = res;
    }catch(e: any){
      console.error(e);
      this.messageService.showError(e)
    } 
  }

  async getMessagesFirst(){
    this.messages = [];
    this.isLoading = true;
    await this.getMessages(1, () => {
      this.setDateSeenNow();
    });
    this.isLoading = false;
    this.scrollToBottom();
  }

  async seeMoreMessages(){
    this.isMoreMessageLoading = true;
    await this.getMessages(this.page + 1);
    this.isMoreMessageLoading = false;
  }

  async getUserInfo(){
    this.isLoadingUser = true;
    try{
      const res: any = await lastValueFrom(this.userService.getUserInfo(this.userId))
      this.user = res;
      console.log(this.user);
    }catch(e: any){
      console.error(e);
      this.messageService.showError(e)
    } 
    this.isLoadingUser = false;
  }

  searchOnEnter(e: any){
    if (e.key === 'Enter' || e.keyCode === 13) {
      
    }
  }

  splitMessages(){
    const result: any[] = [];
    let dateIndex: number = 0;
    let subIndex: number = 0;
    this.messages.forEach((elmt: any, index: number) => {
      const key = this.formatDate(elmt.sentDate);
      const dateEn = this.formatDateEn(elmt.sentDate);
      const secondCheck = (result.length > 0 && result[dateIndex].key !=  key);
      if(result.length == 0 || secondCheck) {
        result.push({
          sub: [
            {
              messages: [elmt],
              userId: elmt.userFromId, 
              ...this.getUtilsSplit(elmt.userFromId)
            }
          ], 
          key,
          dateEn
        });
        subIndex = 0;
        if(secondCheck) dateIndex++;
      } else{
        const dateElmt: any = result[dateIndex];
        const subElmt: any = dateElmt.sub[subIndex];
        if(subElmt.userId == elmt.userFromId)  subElmt.messages.push(elmt);
        else{
          dateElmt.sub.push({
            messages: [elmt],
            userId: elmt.userFromId, 
            ...this.getUtilsSplit(elmt.userFromId)
          })
          subIndex++;
        } 
      }
      
    });
    this.messagesSplited = result;
  }

  formatDate(date: any){
    return moment(date).format("DD/MM/YYYY");
  }

  formatDateEn(date: any){
    return moment(date).format("YYYY-MM-DD");
  }

  formatTime(date: any){
    return moment(date).format("HH:mm");
  }

  getUtilsSplit(userId: string | null){
    const result: any = { };
    if(userId == this.userId){
      result.user = this.user;
      result.left = true;
    } else{
      result.user = this.me;
    }
    return result;
  }

  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }

  isSeen(message: any): boolean{
    if(this.seen){
      return moment(this.seen.lastSeenDate).isAfter(moment(message.sentDate));
    } 
    return false;
  }
  
  getAvatarUrl(chemin: string | null){
    return  chemin ? this.fileService.getFileUrl(chemin) : FileService.defaultAvatarUrl;
  }
  

}
