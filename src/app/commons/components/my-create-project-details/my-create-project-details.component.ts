import { FormData } from '../../../model/form-data';

import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { MessageService } from 'src/app/commons/services/message.service';
import { lastValueFrom } from 'rxjs';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileService } from 'src/app/services/file.service';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { UserService } from 'src/app/services/user.service';
import { environment } from '@env/environment';

@Component({
  selector: 'app-my-create-project-details',
  templateUrl: './my-create-project-details.component.html',
  styleUrls: ['./my-create-project-details.component.scss']
})
export class MyCreateProjectDetailsComponent implements OnInit {

  allRoles: any = environment.allRoles;
  isLoading: boolean = false;
  isSubmitLoading: boolean = false;
  isNextLoading: boolean = false;
  form: FormGroup = new FormGroup({});

  // questions : any = {
  //   parent : "REFERENCEMENT",
  //   categories : [
  //     "ENTREPRISE",
  //     "PROJET",
  //     "ANALYSE_EXISTANT"
  //   ]
  // };
  questions : any = null;
  masterForm: Array<FormGroup>;
  @Input() activeStep : any = null;
  activeStepIndex: number;
  formDatasList : Array<FormData>;
  project : any = null;

  @Input() projectId : string | null = null;
  @Input() clientId: string | null = null;

  userData: any = null;

  constructor(
    private userService: UserService,
    private projectService : ProjectService,
    private messageService: MessageService,
    private router : Router,
    private fb: FormBuilder,
    private activatedRoute : ActivatedRoute,
    private fileService: FileService) { 
      this.activeStepIndex = 0;
      this.masterForm = [];
      this.formDatasList = [];

    }

  ngOnInit(): void {
    this.userData = this.userService.getUserDataFromStorage();
    this.init(); 
  }

  getProgression(){
    return ((this.activeStepIndex + 1)/this.project.type.questions.categories.length)*100
  }
  async init(){
    if(this.projectId){
      await this.fetchData();
    }
    else{
      this.project = this.projectService.getCurrentProject();
      this.project.userId = this.clientId;
    }
    console.log(`Active step : ${this.activeStep}`);
    this.questions = this.project.type.questions;
    console.log(this.questions);
    this.initMasterForm();
    console.log(this.project);
  }


  async fetchData(){
    this.isLoading = true;
    try {
      const result = await lastValueFrom<any>(this.projectService.getById(this.projectId));
      this.project = result;
      console.log('project', this.project);

      // this.setFormValue(this.project);
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }
  trackByFn(index: number): number {
    return index;
  }
  initMasterForm(){
    if(this.questions!=null){
      console.log(this.questions);
      for(let i=0; i<this.questions.categories.length; i++){
        
        let category = this.questions.categories[i];
        this.masterForm.push(new FormGroup({}));
        let formDatas = this.projectService.getFormDatas(this.questions.parent, category);
        if(this.project.answers) formDatas.MODEL = this.project.answers.categories[category];
        this.formDatasList.push(formDatas);
        console.log(category);
        if(this.activeStep==category) this.activeStepIndex = i;
      }
    }
  }


  
  goToStep(step: string): void {
    this.activeStepIndex =
      step === 'prev' ? this.activeStepIndex - 1 : this.activeStepIndex + 1;
  }

  async getAnswers(){
    let answers : any = {
      parent : this.questions.parent,
      categories : {}
    };
    for(let i=0; i<this.questions.categories.length ; i++){
      let formData : FormData = this.formDatasList[i];
      let category : string = this.questions.categories[i];
      let model : any = formData['MODEL'];
      let modelKeys : string[] = Object.keys(model);
      for(let iField=0; iField<modelKeys.length; iField++){
        let field : string = modelKeys[iField];
        if(model[field] instanceof FileList){
          let results : any[]= await this.uploadFiles(model[field]);
          model[field] = results;
          console.log(field);
          console.log(results);
        }
      }
      answers.categories[category] = model;
    }
    return answers;
  }

  async uploadFiles(fileList : FileList){
    if(!fileList) return[];
    let files = [];
    let results : any[]= [];
      for(let i=0; i<fileList.length; i++){
        const fichier = fileList[i];
        if(fichier){
          files.push({file: fichier});
        } 
      }
      const resFiles: any = await lastValueFrom(this.fileService.uploadFiles(files));
      console.log(resFiles);
      resFiles.forEach((file: any) => {
        results.push({chemin: file.path, nom: file.originalname});
      });
      return results;
  }
  
  async next(){
    
    try{
      let form = this.masterForm[this.activeStepIndex];
      if(this.activeStepIndex == this.formDatasList.length){
        await this.finish();
      }
      else{
        if(form.valid){
          //console.log(this.formDatasList[this.activeStepIndex].MODEL);
          //console.log(form.value);
          this.isNextLoading = true;
          this.save();
          this.isNextLoading = false;
          this.goToStep('next');
          
        }
      }
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    
  }

  prev(){
    if(this.projectId!=null && this.activeStepIndex == 0) return;
    if(this.activeStepIndex == 0){
      this.router.navigateByUrl('/create-project-admin/welcome/'+this.clientId);
    }
    else{
      this.goToStep('prev');
    }
    
  }

  async finish(){
    this.isSubmitLoading = true;
    this.save(true);
    this.isSubmitLoading = false;
  }

  async save(finish: boolean= false){
    try{
      this.project.answers = await this.getAnswers();

      if(this.project._id) {
        this.project = await lastValueFrom(this.projectService.updateProjectDetails(this.project)) ;
      }
      else {
        this.project = await lastValueFrom(this.projectService.createProject(this.project)) ;
      }
      if(finish){
        this.projectService.removeCurrentProject();
        if(this.userData.roleId == environment.allRoles.client){
          await this.router.navigateByUrl('/client/project/'+this.project._id);
        } else{
          await this.router.navigateByUrl('/admin/atelier/project/'+this.project._id);
        }
      }
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
  }

  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }

  downloadFileObject(file: File){
    this.fileService.downloadFileObject(file);
  }


}
