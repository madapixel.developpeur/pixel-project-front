import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCreateProjectDetailsComponent } from './my-create-project-details.component';

describe('MyCreateProjectDetailsComponent', () => {
  let component: MyCreateProjectDetailsComponent;
  let fixture: ComponentFixture<MyCreateProjectDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyCreateProjectDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyCreateProjectDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
