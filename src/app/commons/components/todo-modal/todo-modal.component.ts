import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '@env/environment';
import moment from 'moment';
import { lastValueFrom } from 'rxjs';
import { MessageService } from 'src/app/commons/services/message.service';
import { FileService } from 'src/app/services/file.service';
import { ProjectService } from 'src/app/services/project.service';
import { TodoService } from 'src/app/services/todo.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-todo-modal',
  templateUrl: './todo-modal.component.html',
  styleUrls: ['./todo-modal.component.scss']
})
export class TodoModalComponent implements OnInit, OnChanges {
  todoStatus: any = environment.todoStatus;
  isClient: boolean = false;
  todo: any;
  isLoading:boolean= false;
  isEdit: boolean = false;
  isUpdatingTodo: boolean = false;
  @Input() isVisible: boolean = false;
  @Input() todoId: string | null;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  @Output() todoUdpated = new EventEmitter<any>();
  @Input() validateTodo: CallableFunction | null = null;

  form: FormGroup;

  isCommenting: boolean = false;
  fichiers: any[] = [];
  formComments: FormGroup;
  me: any = null;
  comments: any[] = [];
  selectedFiles: File[] = [];
  selectedFilesBase64 : any[] = [];
  isValidating: boolean = false;

  setIsVisible(b: boolean){
    this.isVisible = b;
    this.isVisibleChange.emit(b);
  }
  
  constructor(
    private userService: UserService,
    private fb: FormBuilder, 
    private todoService: TodoService,
    private messageService: MessageService,
    public sanitizer: DomSanitizer,
    private fileService: FileService) { 
      this.form = fb.group({
        name: [null,[Validators.required]],
        description: null,
        apercuFileList: null,
        apercu: null,
        dateLivraison: null,
        progression: null
      })

      this.formComments = fb.group({
        contenu: null,
      });
  }

  ngOnInit(): void {
      this.me = this.userService.getUserDataFromStorage();
      this.isClient = this.me.roleId == environment.allRoles.client; 
  }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes['todoId']){
      this.isEdit = false;
      this.fetchData();
    }
  }
  
  async fetchData(){
    this.isLoading = true;
    try{
      await this.fetchTodo();
      await this.fetchComments();
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isLoading = false;
  }
  async fetchTodo(){
    this.todo = await lastValueFrom(this.todoService.getTodoById(this.todoId));
    console.log(this.todo);
    this.form.setValue({
      name: this.todo.name,
      description: this.todo.description,
      apercuFileList: this.todo.apercuFileList??null,
      apercu: this.todo.apercu??null,
      dateLivraison: this.todo.dateLivraison?moment(this.todo.dateLivraison).format('YYYY-MM-DD') : null,
      progression: this.todo.progression??null
    });
    await this.initApercu();
  }
  
  async initApercu(){
    if(this.todo.apercu==null) return;
    this.selectedFiles = [];
    this.selectedFilesBase64 = [];
    for(let i=0; i<this.todo.apercu.length; i++){
      let filename : string = `Fichier n° ${i}`;
      let blob : any = await this.fileService.getBlob(this.todo.apercu[i].chemin);
      console.log(blob);
      let file : File = new File([blob], filename);
      this.selectedFiles.push(file);
      this.fileService.toBase64(file).then((url: any) => this.selectedFilesBase64.push({
        image : url,
        thumbImage : url
        }));
    }
    this.form.get("apercuFileList")?.patchValue(this.selectedFiles);
    console.log(this.selectedFiles);
    console.log(this.selectedFilesBase64);

  }
  onDelete(index : number) {
    // this.formControl.reset();
    console.log(this.selectedFiles);
    this.selectedFiles.splice(index, 1);

    this.form.get("apercuFileList")?.patchValue(this.selectedFiles);
    console.log("Form Control Value", this.form.get("apercuFileList")?.value);
  }
  onChange(event : any) {
    this.selectedFiles = Array.from(event.target.files);
    console.log(this.selectedFiles);
  }
  getSanitizedImageUrl(file: File) {
    return this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(file)
    );
  }
  isImage(file: File): boolean {
    return /^image\//.test(file.type);
  }

  handleCancel(){
    this.setIsVisible(false);
  }


  toogleEdit(){
    this.isEdit = !this.isEdit;
  }

  async updateTodo(){
    this.isUpdatingTodo = true;
    try{
      const values = this.form.value;
      let apercu : any[]= await this.uploadFiles(this.selectedFiles);
      console.log(apercu);
      values.apercu = apercu;
      values._id = this.todoId;
      this.todo = await lastValueFrom(this.todoService.updateTodoAdmin(values));
      await this.initApercu();
      this.todoUdpated.emit(this.todo);
      this.toogleEdit();
      this.messageService.showSuccess('Tâche modifiée avec succès');
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e);
    }
    this.isUpdatingTodo = false;
  }

  async uploadFiles(fileList : File[]){
    if(!fileList) return[];
    let files = [];
    let results : any[]= [];
      for(let i=0; i<fileList.length; i++){
        const fichier = fileList[i];
        console.log(fichier.name);
        if(fichier){
          files.push({file: fichier});
        } 
      }
      const resFiles: any = await lastValueFrom(this.fileService.uploadFiles(files));
      console.log(resFiles);
      resFiles.forEach((file: any) => {
        results.push({chemin: file.path, nom: file.originalname});
      });
      return results;
  }
  resetFormComments(){
    this.fichiers = [];
    this.formComments.setValue({contenu: null});
  }
  removeFichier(i : number){
    this.fichiers.splice(i, 1);
  }
  onFileSelected(event: any){
    const files = event.target.files;
    for(let i=0; i<files.length; i++){
      const nf = {
        nom: files[i].name,
        chemin: null,
        loading: true
      };
      this.fichiers.push(nf);

      const next =  (res: any) => {
        nf.chemin = res;
      };

      const error = (err: any) => {
        //remove
        const nfIndex = this.fichiers.findIndex((f: any) => f == nf);
        this.fichiers.splice(nfIndex);
        this.messageService.showError(err);
      };

      const complete = () => nf.loading = false;

      this.fileService.uploadFile(files[i]).subscribe({next, error, complete});
    }
    
  }

  async comment(){
    if(this.isCommenting) return;
    this.isCommenting = true;
    try{
      const data = {...this.formComments.value, fichiers: this.fichiers};
      data.todoId = this.todoId;
      console.log(data);
      const res: any = await lastValueFrom(this.todoService.createComment(data))
      res.user = this.me;
      res.user.name = `${this.me.firstName??""} ${this.me.lastName??""}`;
      this.resetFormComments();
      this.loadImages(res);
      this.comments.push(res);
      this.messageService.showSuccess("Commentaire ajouté");
    }catch(e: any){
      console.log(e);
      this.messageService.showError(e)
    } 
    this.isCommenting = false;
  }

  downloadFile(path: string, name: string){
    this.fileService.downloadFile(path, name);
  }

  async fetchComments(){
    this.comments = await lastValueFrom(this.todoService.getComments(this.todoId));
    this.comments.forEach((comment: any) => {
      this.loadImages(comment);
    })
  }


  loadImages(comment: any){
    comment.fichiers.forEach((fichier: any) => {
      if(this.fileService.isImage(fichier.chemin) ){
        fichier.imageUrl = this.fileService.getFileUrl(fichier.chemin);
      }
    });
    
  }

  getAvatarUrl(chemin: string | null){
    return  chemin ? this.fileService.getFileUrl(chemin) : FileService.defaultAvatarUrl;
  }


  async validate(){
    this.isValidating = true;
    if(this.validateTodo){
      this.todo = await this.validateTodo(this.todo);
    }
    this.isValidating = false;
  }
}
