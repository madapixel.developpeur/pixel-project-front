import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SeoAuditServiceService } from 'src/app/services/seo-audit-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  form: FormGroup;
  auditResult: any;

  constructor(private fb: FormBuilder, private seoAuditService: SeoAuditServiceService) {
    this.form = fb.group({
      'website': [null, [Validators.required]],
      'nom': [null, [Validators.required]],
      'email': [null, [Validators.required]],
      'telephone': [null, [Validators.required]],
      'entreprise': [null, [Validators.required]]
    })
  }
  public ngOnInit(): void {
    console.log("Home on Init");
    //this.runAudit("https://tobywallet.fr/", "API_KEY_GOES_HERE");
  }
  onSubmit() {
    const values: any = this.form.value;
    console.log(values);
  }
  

  runAudit(url: string, apiKey: string): void {
    this.seoAuditService.runAudit(url, apiKey)
      .subscribe(result => {
        this.auditResult = result;
        console.log(result);
      }, error => {
        console.log(error);
      });
  }

}
