import { Component } from '@angular/core';
import { ChatService } from './services/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'garage_client';
  

  constructor(private chatService: ChatService){

  }

  ngOnInit(){
    try{
      this.chatService.getNewMessage().subscribe((message: any) => {
        window.dispatchEvent(new CustomEvent(ChatService.EVENT_NAME, {detail: message}));
      })
      this.chatService.getSeen().subscribe((seen: any) => {
        window.dispatchEvent(new CustomEvent(ChatService.SEEN_EVENT_NAME, {detail: seen}));
      })
    } catch(err: any){
      console.error(err);
    }
  }
  
}
