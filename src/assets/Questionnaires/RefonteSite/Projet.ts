import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "raisons_refonte",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle(s) pourrai(en)t être la(les) raison(s) de la refonte?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "description_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous décrire votre cible de façon générale et détaillée? (type de client, âges, comportement, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "description_nouvelle_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Vise-t-on une nouvelle cible pour la refonte? Si oui, pouvez-vous nous la décrire?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "contexte_creation_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Dans quel contexte votre entreprise a-t-elle été créée? (motivation, but) ",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "services_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les différents services/produits/prestations fournis par votre entreprise?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "nouveaux_produits",
        "type": "radio",
        "templateOptions": {
            "label": "Désirez-vous proposer de nouveaux services/produits/prestations?",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "histoire_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est l’histoire derrière votre logo? Quelle est sa signification? Y’aurait-il eu un souci au niveau de l’ancien logo?",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    {
        "key": "identite_nouveau_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle identité voudriez-vous apporter à votre nouveau logo? Pouvez-vous préciser ses spécificités/couleurs/variantes/déclinaisons?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "charte_graphique",
        "type": "file",
        "templateOptions": {
            "label": "Si vous avez une charte graphique, merci de nous le transmettre",
            "placeholder": "",
            "multiple": true
        }
    },
      {
        "key": "techologie_utilisee",
        "type": "select",
        "templateOptions": {
          "label": "Quelle technologie a été utilisée pour le site?",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "CMS"  },
              { "value": 2, "label": "Framework"  },
              { "value": 3, "label": "Base de données"  }
            ]
        }
      },
      {
        "key": "actions_referencements_effectuees",
        "type": "select",
        "templateOptions": {
          "label": "Est-ce que ces actions de référencement suivantes ont déjà été effectuées sur le site?",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "Objectifs mis en place"  },
              { "value": 2, "label": "Actions réalisées"  },
              { "value": 3, "label": "Reporting de résultat (mots-clés; cocon sémantique; positionnement; trafic du site)"  }
            ]
        }
      },

      {
        "key": "outils_utilisees",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels outils avez-vous utilisés pour le suivi de vos positionnements sur Google?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "etat_des_lieux_precedentes_campagnes",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir un rapport sur l’état des lieux des précédentes campagnes?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "objectifs_attendus_refonte",
        "type": "select",
        "templateOptions": {
          "label": "Pouvez-vous nous dire quels peuvent être les objectifs attendus post-refonte?",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "Ton de communication souhaitée"  },
              { "value": 2, "label": "Liste “do” - “don’t”"  },
              { "value": 3, "label": "Fonctionnalités à ajouter"  },
              { "value": 4, "label": "Recherche de nouveau client ET/OU fidélisation"  }
            ]
        }
      },
    {
        "key": "medias",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir les photos/vidéos/fichiers ou tout éléments qui concernent votre entreprise et qui n’ont pas été mentionnés précédemment? (fichiers)",
            "placeholder": "",
            "multiple": true
        }
    }
    
];