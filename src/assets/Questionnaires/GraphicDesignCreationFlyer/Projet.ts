import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l’objectif de votre flyer/affiche ? Que souhaitez-vous communiquer à votre public cible ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le public cible de votre flyer/ affiche ? Qui voulez-vous atteindre avec votre message ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "format_flyer",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le format que vous souhaitez pour votre flyer/affiche ? Quelles sont les dimensions souhaitées ? ",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "contenu",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le contenu que vous souhaitez inclure dans votre flyer/affiche ? Avez- vous des textes ou des images spécifiques que vous souhaitez utiliser ?",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "exigences",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des exigences particulières en termes de design ? Y a-t-il des éléments graphiques que vous souhaitez inclure ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    
];