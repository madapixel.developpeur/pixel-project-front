import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Techniques de référencement : SEO technique";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "architecture_siteweb",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment est structuré votre site web ? Avez-vous une architecture de site claire et cohérente ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "gestion_sitemaps_et_robots",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment gérez-vous vos sitemaps XML et robots.txt pour aider les moteurs de recherche à explorer votre site web ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "mise_en_place_securite_site",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment envisagez-vous la mise en place de la sécurité de votre site web (protocole HTTPS, pare-feu, etc.) ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "exigence_vitesse_de_chargement",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment envisagez-vous la vitesse de chargement de votre site web ? Avez-vous des exigences en termes de temps de chargement ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "exigence_compatibilite_navigateurs",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des exigences particulières en matière de compatibilité avec les navigateurs web et les appareils mobiles ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];