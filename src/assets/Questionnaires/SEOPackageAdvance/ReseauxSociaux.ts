import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Ligne éditoriale : Réseaux sociaux";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "promotion_du_contenu",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment souhaitez-vous promouvoir votre contenu auprès de votre audience?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "directives_specifiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des directives spécifiques pour la publication de contenu sur les réseaux sociaux (hashtags, fréquence de publication, etc.) ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
    {
        "key": "partenaires_potentiels",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà identifié des partenaires potentiels avec lesquels vous pourriez collaborer pour promouvoir votre contenu ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];