import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "nombre_de_decisionnaires",
        "type": "input",
        "templateOptions": {
            "label": "Combien de personnes sont décisionnaires de ce projet? * plus grand le nombre de décisionnaires, plus les délais peuvent être allongés", 
            "type": "number",
            "placeholder": ""
        }
    },{
        "key": "cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous décrire votre cible de façon générale et détaillée? (type de client,comportement, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "contexte_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Dans quel contexte votre entreprise a-t-elle été créée? (motivation, but)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "differentes_services",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les différents services/produits/prestations fournis par votre entreprise?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "histoire_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est l’histoire derrière votre logo? Quelle est sa signification?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "souci_ancien_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Y’aurait-il eu un souci au niveau de l’ancien logo? Si oui, quel(s) était(en)t-il(s)?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "charte_graphique",
        "type": "file",
        "templateOptions": {
            "label": "Si vous avez une charte graphique, merci de nous le transmettre",
            "placeholder": "",
            "multiple": true
        }
    },
    {
        "key": "identite_nouveau_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle identité voudriez-vous apporter à votre nouveau logo? Pouvez-vous préciser ses spécificités/couleurs/variantes/déclinaisons?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "support_de_communication",
        "type": "file",
        "templateOptions": {
            "label": "Disposez-vous de supports de communication? Si oui, pouvez-vous nous fournir les visuels déjà conçus ? Si non, Pouvez-vous nous fournir des visuels/photos/couleurs qui expliquent au mieux vos attentes",
            "placeholder": "",
            "multiple": true
        }
    },
    {
        "key": "objectif_branding",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous détailler les objectifs attendus du branding/rebranding? ton de communication souhaitée, liste “do” - “don’t”, fonctionnalités à ajouter, recherche de nouveau client ET/OU fidélisation",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    {
        "key": "autres_elements_necessaires",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir les photos/vidéos/fichiers ou tout éléments qui concernent votre entreprise et qui n’ont pas été mentionnés précédemment? ",
            "placeholder": "",
            "multiple": true
        }
    }
];