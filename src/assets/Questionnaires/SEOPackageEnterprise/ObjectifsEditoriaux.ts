import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Ligne éditoriale : Objectifs éditoriaux";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_editoriaux",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les objectifs éditoriaux de votre site (informer, vendre, persuader, divertir, etc.) ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "messages_a_transmettre",
        "type": "textarea",
        "templateOptions": {
            "label": " Quels sont les messages clés que vous souhaitez transmettre à votre public cible ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
];