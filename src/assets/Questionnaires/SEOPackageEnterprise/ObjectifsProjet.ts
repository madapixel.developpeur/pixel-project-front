import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Les objectifs du projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "details_objectifs_attendus",
        "type": "textarea",
        "templateOptions": {
            "label": "Détaillez le ou les objectifs attendus avec ce projet de référencement",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "objectif_couverture_locale",
        "type": "textarea",
        "templateOptions": {
            "label": "Détaillez les objectifs de la couverture locale",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "zones_geographiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles zones géographiques voulez-vous toucher ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    }
];