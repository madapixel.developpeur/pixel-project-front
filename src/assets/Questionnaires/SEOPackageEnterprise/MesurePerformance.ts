import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Suivi et analyse : Mesure de la performance";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "optimisation_actuelle",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les indicateurs clés de performance que vous souhaitez suivre pour évaluer le succès de la stratégie de référencement ? (Nombre de vue, clics, taux de conversions, …)",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    
];