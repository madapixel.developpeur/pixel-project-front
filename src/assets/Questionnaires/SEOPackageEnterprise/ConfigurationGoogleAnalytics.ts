import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Suivi et analyse : Configuration de Google Analytics";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "configuration_google_analytics",
        "type": "input",
        "templateOptions": {
            "label": "Avez-vous déjà configuré Google Analytics pour votre site web ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "informations_connexion",
        "type": "textarea",
        "templateOptions": {
            "label": "Si oui, pouvez-vous fournir à l'agence les informations de connexion pour accéder au compte ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "configuration_agence",
        "type": "textarea",
        "templateOptions": {
            "label": "Si non, l'agence doit-elle configurer Google Analytics pour vous ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "objectifs_de_suivi",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les objectifs de suivi que vous souhaitez mettre en place dans Google Analytics ? (suivi des conversions, suivi des événements, suivi du commerce électronique, etc.)",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    
];