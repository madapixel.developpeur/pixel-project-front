import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Techniques de référencement : Optimisation on-page";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "optimisation_actuelle",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment votre site est-il actuellement optimisé pour les moteurs de recherche ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "pages_a_optimiser",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les pages principales que vous souhaitez optimiser ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "elements_a_ameliorer",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les éléments à améliorer sur votre site en termes de contenu, structure et balisage ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "directives_specifiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des directives spécifiques pour la rédaction de contenu ou pour la structure de votre site ?",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
];