import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_du_projet",
        "type": "textarea",
        "templateOptions": {
            "label": "Description et objectif du projet",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "etude_de_marche",
        "type": "file",
        "templateOptions": {
            "label": "Télécharger l'étude de marché",
            "placeholder": "",
            "multiple" : true
        }
    },
    {
        "key": "cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister vos cibles",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "description_cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous décrire votre clientèle de façon précise et détaillée? (âge, homme/femme, communauté, catégorie socio-professionnel, situation matrimoniale, comportement, etc.)",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "problemes_rencontres",
        "type": "textarea",
        "templateOptions": {
            "label": "Problème(s) rencontré(s) - solution",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "temoignages_client",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister 3 témoignages client",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "questions",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister 5 questions fréquemment posées",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "description_offre",
        "type": "textarea",
        "templateOptions": {
            "label": "Décrire votre offre (catalogue - mono produit/service, plusieurs produits/services; prix, nom de la référence, produit assujetti à la TVA) ",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "modele_facture",
        "type": "file",
        "templateOptions": {
            "label": "Télécharger un exemple ou modèle de facture (.pdf)",
            "placeholder": "",
            "multiple" : true,
            "accept" : ".pdf"
        }
    },
    {
        "key": "charte_graphique",
        "type": "file",
        "templateOptions": {
            "label": "Si vous avez une charte graphique, merci de nous le transmettre",
            "placeholder": "",
            "multiple" : true
        }
    },
];