import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l’objectif principal de la brochure ? S’agit-il de présenter une nouvelle gamme de produits ou de services, de promouvoir un événement ou de renforcer l’image de marque de l’entreprise ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui est votre public cible pour cette brochure ? Quel est leur âge, leur sexe, leur profession, leur niveau d’éducation et leurs centres d’intérêt ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "message",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le message clé que vous souhaitez transmettre avec cette brochure? Comment voulez-vous que votre public cible perçoive votre entreprise ?",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "format_brochure",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le format et la taille souhaités pour la brochure ? Souhaitez-vous une brochure pliée en deux, en trois ou en quatre ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "preferences_design",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des préférences en termes de design ? Quels sont les éléments graphiques que vous souhaitez intégrer, tels que des images, des logos ou des couleurs spécifiques ? ",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "contraintes",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des contraintes de budget ou de délais pour la création de la brochure ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "contenu",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le contenu que vous souhaitez inclure dans la brochure ? Avez-vous déjà rédigé le texte ou souhaitez-vous que l’équipe de création s’en occupe ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "exemples_brochures",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des exemples de brochures que vous appréciez ? Pourquoi les trouvez-vous attractifs et inspirants ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    
];