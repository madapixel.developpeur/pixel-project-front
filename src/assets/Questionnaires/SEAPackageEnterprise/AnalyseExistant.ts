import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse de l’existant pour le site internet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "etat_des_lieux",
        "type": "textarea",
        "templateOptions": {
            "label": "Etat des lieux des précédentes campagnes",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "nombre_de_personnes_engagees",
        "type": "input",
        "templateOptions": {
            "label": "Combien de personnes sont engagées sur ce projet ?",
            "placeholder": "",
            "type":"number",
            "required": false
        }
    },
    {
        "key": "position_actuelle",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment décririez-vous la position actuelle de votre entreprise sur le marché ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "caracteristiques_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les principales caractéristiques de votre public cible ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "campagnes_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà réalisé des campagnes publicitaires en dehors du SEA (par exemple, sur les réseaux sociaux?)",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "problemes_rencontres",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les problèmes que vous avez rencontrés avec vos campagnes publicitaires précédentes ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];