import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Présenter l’entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "date_de_creation",
      "type": "datepicker",
      "templateOptions": {
        "label": "Date de création",
        "placeholder": "",
        "required": false
      }
    },
    {
        "key": "activite_principale",
        "type": "input",
        "templateOptions": {
            "label": "Activité principale",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "services_ou_produits_vendus",
        "type": "textarea",
        "templateOptions": {
            "label": "Services ou produits vendus",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Principaux concurrents",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    }
];