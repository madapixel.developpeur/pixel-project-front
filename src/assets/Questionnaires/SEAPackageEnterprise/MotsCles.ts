import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Mots-clés";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "liste_mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous une liste de mots-clés que vous souhaitez cibler pour cette campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "origines_mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment avez-vous choisi ces mots-clés ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "mots_cles_exceptions",
        "type": "textarea",
        "templateOptions": {
            "label": "Y a-t-il des mots-clés spécifiques que vous ne souhaitez pas cibler pour votre campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
   

];