import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concurrents";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents dans votre secteur d'activité ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "campagnes_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels types de campagnes publicitaires en ligne utilisent-ils actuellement ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "idees_pour_se_demarquer",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des idées pour vous démarquer de vos concurrents dans votre campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
   

];