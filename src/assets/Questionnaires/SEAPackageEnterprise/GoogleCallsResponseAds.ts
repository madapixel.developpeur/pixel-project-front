import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Campagne : Google Calls Response Ads";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "visuels_creatifs",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà des visuels créatifs prêts à être utilisés pour votre campagne publicitaire Google Display Ads ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "diffusion_annonces",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment souhaitez-vous que vos annonces soient ciblées et diffusées aux visiteurs du réseau Google Display ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },

   

];