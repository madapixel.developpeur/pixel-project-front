import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Détails du site";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "monnaie",
        "type": "input",
        "templateOptions": {
            "label": "Quelle monnaie souhaiteriez-vous pour les transactions sur votre site? (Euro, Dollar, …)",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "langue",
        "type": "input",
        "templateOptions": {
            "label": "Quelles sont les langues que vous voulez proposer sur votre site?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "informations_client_a_recuperer",
        "type": "input",
        "templateOptions": {
            "label": "Quelles sont les informations que vous voulez récupérer lorsque votre client vous contacte via le formulaire de contact? (Nom, prénom, Adresse mail, …)",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "charte_graphique",
        "type": "file",
        "templateOptions": {
            "label": "Si vous avez une charte graphique, merci de nous la transmettre.",
            "multiple" : true,
            "placeholder": ""
        }
    },
    {
        "key": "nombre_de_page",
        "type": "input",
        "templateOptions": {
            "label": "Combien de pages souhaitez-vous pour le site?",
            "placeholder": "",
            "type": "number",
            "required": false
        }
    },
    {
        "key": "intitules_pages",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel serait l’intitulé de ces pages? (barre de navigation ou entrée de menu) Ex : accueil - nos services - contact, etc. ",
            "placeholder": "",
            "required": false
        }
    },

];