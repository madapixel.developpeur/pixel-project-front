import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Complétez les coordonnées à propos de votre entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "nom_societe",
        "type": "input",
        "templateOptions": {
            "label": "Nom de la société (mentionner ici s'il existe un url vers votre site)",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "contexte_creation",
        "type": "textarea",
        "templateOptions": {
            "label": "Dans quel contexte votre entreprise a-t-elle été créée? (motivation, but)",
            "placeholder": "",
            "rows": 6,
            "required": false
        }
    },
    {
        "key": "services_et_produits_fournis",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les différents services/produits/prestations fournis par votre entreprise?",
            "placeholder": "",
            "rows": 5,
            "required": false
        }
    },
    {
        "key": "histoire_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est l’histoire derrière votre logo? Quelle est sa signification?",
            "placeholder": "",
            "rows": 4,
            "required": false
        }
    },
    {
        "key": "adresse",
        "type": "input",
        "templateOptions": {
            "label": "Adresse",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "contact",
        "type": "input",
        "templateOptions": {
            "label": "Contact (numéro téléphone et adresse mail)",
            "placeholder": "",
            "required": false
        }
    },
    {
      "key": "date_de_creation",
      "type": "datepicker",
      "templateOptions": {
        "label": "Date de création",
        "placeholder": "Entrez la date de création de l'entreprise",
        "required": false
      }
    },
    {
        "key": "activite_principale",
        "type": "input",
        "templateOptions": {
            "label": "Activité principale",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "lien_reseaux_sociaux",
        "type": "textarea",
        "templateOptions": {
            "label": "Lien réseaux sociaux",
            "placeholder": "Entrez le(s) lien(s) réseaux sociaux'",
            "required": false,
            "rows":5
        }
    },
    {
        "key": "conditions_generales_de_vente",
        "type": "file",
        "templateOptions": {
            "label": "Conditions générales de vente (.pdf)",
            "placeholder": "Entrez le conditions générales de vente",
            "multiple": true,
            "accept" : ".pdf"
        }
    },
    {
        "key": "mentions_legales",
        "type": "file",
        "templateOptions": {
            "label": "Mentions légales (.pdf)",
            "placeholder": "Entrez le mentions légales",
            "accept" : ".pdf"
        }
    },
    {
        "key": "nombre_de_salaries",
        "type": "input",
        "templateOptions": {
            "label": "Nombre de salariés",
            "type": "number",
            "placeholder": "Entrez le nombre de salariés"
        }
    }
];