import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le site";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "intitules_pages",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel serait l’intitulé de ces pages? (barre de navigation ou entrée de menu) Ex : accueil - nos services - contact, etc.",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "elements_redactionnels",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous transmettre les éléments rédactionnels concernant chacune de ces pages?",
            "multiple" : true,
            "placeholder": ""
        }
    },
    {
        "key": "objectifs_attendus",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous détailler les objectifs attendus de la création du site? ton de communication souhaitée liste “do” - “don’t” recherche de nouveau client ET/OU fidélisation",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
      {
        "key": "medias",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir les photos/vidéos/fichiers ou tout éléments qui concernent votre entreprise et qui n’ont pas été mentionnés précédemment? (fichiers)",
            "placeholder": "",
            "multiple": true
        }
    },
];