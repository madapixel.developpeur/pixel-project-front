import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Optimisation de l'application";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "optimisation_titre",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà optimisé les titres, descriptions et mots-clés de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "ressources_visuelles",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des ressources visuelles de qualité professionnelle pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    {
        "key": "mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots-clés que vous souhaitez cibler pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "mesure_performance",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment mesurez-vous actuellement les performances de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    
];