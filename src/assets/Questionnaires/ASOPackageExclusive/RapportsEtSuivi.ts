import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Rapports et suivi";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "types_rapports_et_suivi",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les types de rapports et de suivi que vous souhaitez recevoir pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "indicateurs_performance",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les indicateurs de performance clés que vous souhaitez suivre pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    
];