import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Public cible et utilisateurs";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment décririez-vous votre public cible ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "utilisateurs_actuels",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos utilisateurs actuels ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    {
        "key": "appreciations_utilisateurs",
        "type": "textarea",
        "templateOptions": {
            "label": "Qu'est-ce que vos utilisateurs apprécient le plus dans votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    
];