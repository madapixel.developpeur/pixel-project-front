import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Spécificités et livrables : Le contenu de votre site";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "contenus_a_ameliorer",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister les contenus que l’agence doit reprendre / améliorer / créer",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    {
        "key": "achat_photo",
        "type": "input",
        "templateOptions": {
            "label": "L’agence doit-elle prévoir l’achat de photo ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "format_contenu_proposes",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister également le format des contenus proposés (Texte, Photo, Audio, Vidéo, 3D)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];