import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Les cibles";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "entreprises_ou_particuliers",
        "type": "input",
        "templateOptions": {
            "label": "Votre site est-il pour les entreprises ou les particuliers?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "description_audience",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous faire une description de l'audience cible du projet ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "caracteristiques_demographiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Donnez leurs caractéristiques démographiques et psychographiques",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "besoins_et_attentes",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous énumérer leurs besoins et leurs attentes.",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];