import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse de l’existant pour le site internet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "solution_utilisee",
        "type": "textarea",
        "templateOptions": {
            "label": "Solution utilisée actuellement",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "version_solution",
        "type": "input",
        "templateOptions": {
            "label": "Version de la solution",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "hebergement_utilise",
        "type": "input",
        "templateOptions": {
            "label": "Hébergement utilisé",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "statistiques_actuelles",
        "type": "textarea",
        "templateOptions": {
            "label": "Les statistiques actuelles (trafic mensuel, taux de rebond, taux de conversion)",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "nombre_de_pages",
        "type": "input",
        "templateOptions": {
            "label": "Nombre de pages",
            "type": "number",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "type_de_site",
        "type": "input",
        "templateOptions": {
            "label": "Le type de site (entreprise, e-commerce, plateforme etc.)",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "campagnes_de_referencement",
        "type": "textarea",
        "templateOptions": {
            "label": "Les campagnes de référencement déjà engagées",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "plugins_utilises",
        "type": "textarea",
        "templateOptions": {
            "label": "Les plugins utilisés",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "nombre_de_personnes_engagees",
        "type": "input",
        "templateOptions": {
            "label": "Combien de personnes sont engagées sur ce projet ?",
            "type": "number",
            "placeholder": "",
            "required": false
        }
    },
];