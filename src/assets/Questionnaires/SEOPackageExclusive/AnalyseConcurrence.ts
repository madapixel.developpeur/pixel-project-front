import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse de la concurrence";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "evaluation_concurrence",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment évaluez-vous la concurrence dans votre secteur d'activité ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
    {
        "key": "mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots-clés que vos concurrents ciblent et sur lesquels vous souhaitez vous positionner également ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
];