import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Ligne éditoriale : Contenu éditorial";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "types_de_contenus",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les types de contenus que vous souhaitez proposer sur votre site (articles de blog, guides, infographies, vidéos, etc.) ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "frequence_de_publication_souhaitee",
        "type": "input",
        "templateOptions": {
            "label": "Quelle est la fréquence de publication souhaitée pour chaque type de contenu ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "ton_editorial",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le ton éditorial que vous souhaitez adopter (sérieux, humoristique, décontracté, etc.) ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "sujets_prioritaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les sujets prioritaires sur lesquels vous souhaitez publier du contenu ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];