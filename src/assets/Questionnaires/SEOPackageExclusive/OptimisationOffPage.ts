import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Techniques de référencement : Optimisation off-page";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "strategie_netlinking",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous actuellement une stratégie de netlinking en place ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "profil_de_liens_actuel",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le profil de liens actuel de votre site (nombre, qualité, pertinence) ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "evolution_strategie_netlinking",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment souhaitez-vous que votre stratégie de netlinking évolue à l'avenir ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "penalites",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà été pénalisés par les moteurs de recherche ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },];