import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Présenter l’entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "nom_de_la_societe",
        "type": "textarea",
        "templateOptions": {
            "label": "Nom de la société (mentionner ici s'il existe un url vers votre site)",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
      "key": "date_de_creation",
      "type": "datepicker",
      "templateOptions": {
        "label": "Date de création",
        "placeholder": "",
        "required": false
      }
    },
    {
        "key": "type_entreprise",
        "type": "select",
        "templateOptions": {
          "label": "Type",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "SARL"  },
              { "value": 2, "label": "EURL"  },
              { "value": 3, "label": "EI"  },
              { "value": 4, "label": "SAS"  }
            ]
        }
      },
      {
        "key": "produits_et_services",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels produits ou services proposez-vous ? ",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "valeurs_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est la valeur de votre entreprise ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "logo",
        "type": "file",
        "templateOptions": {
            "label": "Logos",
            "placeholder": "",
            "multiple": true
        }
    },
    {
        "key": "valeurs",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est la valeur de votre entreprise ? ",
            "placeholder": "",
            "required": false,
            "rows": 2
        }
    },
    {
        "key": "slogan",
        "type": "textarea",
        "templateOptions": {
            "label": "Slogan",
            "placeholder": "",
            "required": false,
            "rows": 2
        }
    },
    {
        "key": "organigramme",
        "type": "file",
        "templateOptions": {
            "label": "Organigramme",
            "placeholder": "",
            "multiple": true
        }
    },
];