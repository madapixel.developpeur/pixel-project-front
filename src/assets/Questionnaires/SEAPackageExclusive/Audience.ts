import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Audience";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "canaux_marketing",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les canaux de marketing que votre public cible utilise le plus souvent ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "audience_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est votre audience cible pour cette campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "profil_demographique",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le profil démographique de votre audience cible ? (Âge, genre, lieu, etc.)",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "interets_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les intérêts ou les comportements communs de votre audience cible ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },

];