import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectif_principal",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l'objectif principal de votre campagne publicitaire Google Ads ? Est-ce d'augmenter les ventes, les conversions, la notoriété de la marque, ou autre chose ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "objectif_roi",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous un objectif de retour sur investissement (ROI) spécifique pour votre campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "mesure_campagnes_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment mesurez-vous actuellement le succès de vos campagnes publicitaires en ligne ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },

];