import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Campagne : Google Leads Ads Campaign";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "redirection_landing_page",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment souhaitez-vous que les prospects intéressés par vos produits ou services soient redirigés vers votre site web ou votre landing page ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "nouvelles_offres",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà une idée de l'offre que vous souhaitez mettre en avant pour attirer les prospects ?",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },

   

];