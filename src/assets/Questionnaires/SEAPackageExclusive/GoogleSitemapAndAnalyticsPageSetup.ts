import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Campagne : Google Calls Response Ads";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "configuration_google_analytics",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà configuré un compte Google Analytics pour votre site web ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "plan_de_site",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà un plan de site en place pour votre site web ou avez-vous besoin d'aide pour en créer un ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },

   

];