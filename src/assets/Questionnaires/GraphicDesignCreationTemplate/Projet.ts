import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectif",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l’objectif principal de votre site web ? Est-ce de vendre des produits ou services, de présenter votre entreprise ou de fournir des informations à vos visiteurs ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui est votre public cible ? Quel est le profil de votre audience ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "type_ambiance",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le style et l’ambiance que vous souhaitez donner à votre site web ? Avez-vous des préférences en matière de couleurs, de typographie ou d’images ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "fonctionnalites",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les fonctionnalités spécifiques que vous souhaitez inclure dans votre site web, telles que des formulaires de contact, des pages de témoignages clients ou des galeries d’images ? ",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "delai",
        "type": "input",
        "templateOptions": {
            "label": "Quels sont vos délais pour la création de votre site web ? Y a-t-il des dates butoirs à respecter ? ",
            "placeholder": "",
            "required": false
        }
    },
    
];