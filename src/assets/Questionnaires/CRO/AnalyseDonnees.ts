import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse des données";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "donnees_comportements_utilisateurs",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des données sur le comportement des utilisateurs de votre site web (ex. Google Analytics) ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "utilisation_des_donnees",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment utilisez-vous ces données pour comprendre les problèmes de conversion sur votre site web ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "pages_faibles_taux_de_conversion",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous identifié les pages de votre site web qui ont les plus faibles taux de conversion ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "elements_a_tester",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les éléments de votre site web que vous souhaitez tester pour améliorer le taux de conversion (ex. la mise en page, les couleurs, les titres, les appels à l'action, etc.) ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];