import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyses comportements";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "experience_utilisateur_actuel",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment évaluez-vous l'expérience utilisateur actuelle de votre site web ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "enquetes_utilisateurs",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà mené des enquêtes auprès des utilisateurs pour évaluer leur expérience sur votre site web ? ",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "points_de_friction",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les principaux points de friction ou les obstacles que les utilisateurs rencontrent sur votre site web ? ",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "canaux_de_trafic",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les canaux de trafic qui apportent le plus de visiteurs à votre site web ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "qualite_trafic",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà évalué la qualité de votre trafic entrant (ex. la provenance, les intérêts, les comportements, etc.) ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "pages_de_destination_succes",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les pages de destination qui reçoivent le plus de trafic et comment performantes sont-elles en termes de taux de conversion ? ",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];