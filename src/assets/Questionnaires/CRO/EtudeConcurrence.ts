import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Étude de la concurrence";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents en ligne et comment se comparent-ils à votre taux de conversion ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "strategie_concurrent",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment vos concurrents optimisent-ils leur site web pour améliorer leur taux de conversion ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];