import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Conception du site web";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "etat_actuel_siteweb",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l'état actuel de votre site web en termes de convivialité, de vitesse de chargement, de qualité du contenu, de structure de la page, etc. ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "tests_utilisabilite",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà effectué des tests d'utilisabilité sur votre site web pour identifier les problèmes de convivialité ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];