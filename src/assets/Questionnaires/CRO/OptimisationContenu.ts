import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Optimisation du contenu";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "pages_a_optimiser",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les pages ou les sections de votre site web que vous souhaitez optimiser pour la conversion ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "amelioration_contenu",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment pouvez-vous améliorer le contenu de votre site web pour augmenter le taux de conversion (ex. en ajoutant des témoignages clients, des images, des vidéos, des appels à l'action plus clairs, etc.) ? ",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];