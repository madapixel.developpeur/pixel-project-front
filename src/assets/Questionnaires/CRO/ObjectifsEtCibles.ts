import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs et cibles";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_pour_marketing",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont vos objectifs de marketing en ligne en termes de conversion ? (ex. augmenter les ventes, augmenter les inscriptions, augmenter le temps passé sur le site web, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "taux_de_conversion",
        "type": "input",
        "templateOptions": {
            "label": "Quel est le taux de conversion actuel de votre site web ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "public_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est votre public cible ? Quel est le profil type de votre client idéal ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "mesure_de_performance",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment mesurez-vous actuellement les performances de votre site web en termes de conversion ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
];