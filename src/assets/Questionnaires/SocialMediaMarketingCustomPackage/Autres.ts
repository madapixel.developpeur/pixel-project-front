import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Autres";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_visibilites",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est votre objectif en terme de visibilité : acquisition de nouveau client ou fidélisation ?",
            "placeholder": "",
            "required": false,
            "rows":5
        }
    },
    {
        "key": "frequence_publications",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle fréquence de publications souhaitez-vous ? ( Nombre de publications hebdomadaires et mensuelles )",
            "placeholder": "",
            "required": false,
            "rows":2
        }
    },
    {
        "key": "type_contenu",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel type de contenu souhaitez-vous publier ? ",
            "placeholder": "",
            "required": false,
            "rows":2
        }
    },
    {
        "key": "objectifs_marque",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l’objectif de votre marque ?",
            "placeholder": "",
            "required": false,
            "rows":5
        }
    },
    {
        "key": "ton_communication",
        "type": "select",
        "templateOptions": {
          "label": "Quel ton de communication souhaitez-vous communiquer sur les réseaux sociaux ",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "ton humoristique"  },
              { "value": 2, "label": "ton ironique"  },
              { "value": 3, "label": "ton dramatique"  },
              { "value": 4, "label": "ton polémique"  },
              { "value": 5, "label": "ton poétique"  },
              { "value": 6, "label": "ton dialectique"  },
              { "value": 7, "label": "ton familier"  }
            ]
        }
      },
      {
        "key": "sujets_a_aborder",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les sujets à aborder : les do ( exemple : produits - entreprise - employés ), les don’t ( exemple : politique - environnement - concurrents )",
            "placeholder": "",
            "required": false,
            "rows":7
        }
    },
    {
        "key": "message_principal",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le message principal que vous voulez transmettre à votre cible ?",
            "placeholder": "",
            "required": false,
            "rows":4
        }
    },
    {
        "key": "force_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est la force de votre entreprise ?",
            "placeholder": "",
            "required": false,
            "rows":4
        }
    },
    {
        "key": "documents",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous vous fournir les documents concernant votre entreprise : historique – détails des produits et services ?",
            "placeholder": "",
            "multiple" : true
        }
    },
    {
        "key": "cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Définir votre cible : ses habitudes, ses motivations d’achat, son genre : féminin - masculin, tranche d’âge, son budget, sa localisation",
            "placeholder": "",
            "required": false,
            "rows":5
        }
    },
    {
        "key": "concurrents_directs",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos concurrents directs ? ",
            "placeholder": "",
            "required": false,
            "rows":2
        }
    },
    {
        "key": "duree_prestation",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est la durée de la prestation souhaitée ? ",
            "placeholder": "",
            "required": false,
            "rows":2
        }
    },
    {
        "key": "budget_publicites",
        "type": "radio",
        "templateOptions": {
            "label": "Souhaitez-vous allouer un budget pour les publicités ? ",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "travail_avec_influenceurs",
        "type": "radio",
        "templateOptions": {
            "label": "Travaillez-vous avec des influenceurs ? ",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "travail_avec_autres_marques",
        "type": "radio",
        "templateOptions": {
            "label": "Travaillez-vous avec d’autres marques ?",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "charte_graphique",
        "type": "textarea",
        "templateOptions": {
            "label": "Votre charte graphique, quel type de visuel aimerez-vous : style : graphique - photo - illustration - citation graphique - minimaliste - couleurs dominantes",
            "placeholder": "",
            "required": false,
            "rows":7
        }
    },
    {
        "key": "charte_graphique",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir les photos et vidéos disponibles sur votre entreprise que nous pouvons exploiter ? ",
            "placeholder": "",
            "multiple" : true
        }
    },

];