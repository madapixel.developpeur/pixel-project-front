import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le nom de votre entreprise et quel est son domaine d’activité ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "identite_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment décririez-vous l’identité de votre entreprise (valeurs, mission, vision, personnalité, etc.) ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui est votre public cible ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "vision_public",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment voulez-vous que votre entreprise soit perçue par votre public cible ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "idee_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous une idée de ce que vous voulez pour votre logo (couleurs, formes, typographie, etc.) ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "logo_exceptions",
        "type": "textarea",
        "templateOptions": {
            "label": "Y a-t-il des logos que vous aimez ou que vous n’aimez pas ? Pourquoi ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos concurrents s’il y en a ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },

    {
        "key": "support",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les supports sur lesquels le logo sera utilisé (papier à en-tête, site web, réseaux sociaux, etc.) ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "exigences",
        "type": "textarea",
        "templateOptions": {
            "label": "Y a-t-il des exigences techniques pour la création du logo (format, résolution, etc.) ? ",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "budget",
        "type": "input",
        "templateOptions": {
            "label": "Quel est votre budget pour la création du logo ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "delai",
        "type": "input",
        "templateOptions": {
            "label": "Quel est le délai de réalisation souhaité ?",
            "placeholder": "",
            "required": false
        }
    },
    
];