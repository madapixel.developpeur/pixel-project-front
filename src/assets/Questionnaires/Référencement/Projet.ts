import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_du_projet",
        "type": "textarea",
        "templateOptions": {
            "label": "Description du projet",
            "placeholder": "Entrez description du projet",
            "rows":5,
            "required" : false
        }
    },
    {
        "key": "objectif_du_campagne_de_referencement",
        "type": "textarea",
        "templateOptions": {
            "label": "Objectif du campagne de référencement",
            "placeholder": "Entrez objectif du campagne de référencement",
            "rows":5,
            "required" : false
        }
    },
    {
        "key": "lien_du_site",
        "type": "input",
        "templateOptions": {
            "label": "Lien du site",
            "placeholder": "Entrez le lien du site'",
            "required": false
        },
        validation: {
            messages: {
              pattern: (error, field: FormlyFieldConfig) =>
                `"${field.formControl?.value}" est un lien invalide.`
            }
          },
          expressionProperties: {
            "templateOptions.pattern": of(new RegExp("^(?:(?:https?|ftp):\/\/)?[^\s/$.?#].[^\s]*$"))
          }
    }
];