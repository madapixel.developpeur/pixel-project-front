import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Les projets et outils";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "outils_utilisees",
      "type": "textarea",
      "templateOptions": {
        "label": "Quels outils avez-vous utilisés pour le suivi de vos positionnements sur Google?",
        "placeholder": "",
        "required": false,
        "rows" : 5
      }
    },
    {
        "key": "nombre_personnes_engagees",
        "type": "input",
        "templateOptions": {
            "label": "Combien de personnes sont engagées sur le projet?",
            "placeholder": "",
            "type": "number",
        }
    }
];