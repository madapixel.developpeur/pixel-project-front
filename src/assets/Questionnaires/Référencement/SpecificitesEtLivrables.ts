import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Spécificités et livrables, en termes de contenu de site";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "axes_a_ameliorer",
      "type": "textarea",
      "templateOptions": {
        "label": "Pouvez-vous lister les axes/contenus que le prestataire doit reprendre/améliorer/créer?",
        "placeholder": "",
        "required": false,
        "rows" : 5
      }
    },
    {
        "key": "creation_de_texte",
        "type": "radio",
        "templateOptions": {
            "label": "Le prestataire doit-il prévoir la création de texte?",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "solutions_utilisees_referencement_suivi",
        "type": "textarea",
        "templateOptions": {
          "label": "Quelles seraient les solutions utilisées pour le référencement et le suivi?",
          "placeholder": "",
          "required": false,
          "rows" : 5
        }
      },
      {
        "key": "besoins_connexes_au_projet",
        "type": "select",
        "templateOptions": {
          "label": "Pouvez-vous préciser vos attentes concernant les besoins connexes à ce projet que le prestataire devra fournir?",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "Héberger votre site Internet"  },
              { "value": 2, "label": "Assurer le suivi"  },
              { "value": 3, "label": "Editer votre site"  },
              { "value": 4, "label": "Formation à l’utilisation des services de suivi"  }
            ]
        }
      }
];