import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Pour l’analyse de l’existant pour le site Internet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "solution_utilisee",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est la solution utilisée actuellement? Quelle version de la solution est-utilisée? Quel hébergement est-il utilisé?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "statistiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les statistiques actuelles du site? trafic mensuel, taux de rebond, taux de conversion ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "nombre_de_page",
        "type": "input",
        "templateOptions": {
            "label": "Quel est le nombre de pages du site?",
            "type": "number",
            "placeholder": "Entrez le nombre de pages du site",
            "required": false
        }
    },
    {
        "key": "liste_de_plugins",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les plugins utilisés?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "nombre_personnes_engagees",
        "type": "input",
        "templateOptions": {
            "label": "Combien de personnes sont engagées sur le projet?",
            "type": "number",
            "placeholder": "",
            "required": false
        }
    },
];