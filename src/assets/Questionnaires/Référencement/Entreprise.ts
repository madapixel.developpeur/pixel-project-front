import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Complétez les coordonnées à propos de votre entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "date_de_creation",
      "type": "datepicker",
      "templateOptions": {
        "label": "Date de création",
        "placeholder": "Entrez la date de création de l'entreprise",
        "required": false
      }
    },
    {
        "key": "activite_principale",
        "type": "input",
        "templateOptions": {
            "label": "Activité principale",
            "placeholder": "Entrez l'activité principale'",
            "required": false
        }
    },
    {
        "key": "principaux_axes_de_developpement",
        "type": "textarea",
        "templateOptions": {
            "label": "Principaux axes de développement",
            "placeholder": "Entrez principaux axes de développement",
            "rows":5
        }
    },
    {
        "key": "nombre_de_salaries",
        "type": "input",
        "templateOptions": {
            "label": "Nombre de salariés",
            "type": "number",
            "placeholder": "Entrez le nombre de salariés"
        }
    },
    {
        "key": "lien_du_site_internet",
        "type": "input",
        "templateOptions": {
            "label": "Lien du site Internet",
            "placeholder": "Entrez le lien du site Internet'",
            "required": false
        },
        validation: {
            messages: {
              pattern: (error, field: FormlyFieldConfig) =>
                `"${field.formControl?.value}" est un lien invalide.`
            }
          },
          expressionProperties: {
            "templateOptions.pattern": of(new RegExp("^(?:(?:https?|ftp):\/\/)?[^\s/$.?#].[^\s]*$"))
          }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Principaux concurrents",
            "placeholder": "Entrez principaux concurrents",
            "rows":3
        }
    }
];