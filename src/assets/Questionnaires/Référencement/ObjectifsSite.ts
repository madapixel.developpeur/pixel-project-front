import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs du site";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "objectifs_attendus",
      "type": "textarea",
      "templateOptions": {
        "label": "Pouvez-vous détailler les objectifs attendus du projet de référencement? Ex : améliorer le taux de conversion - améliorer l’acquisition de trafic via le référencement naturel/sponsorisé",
        "placeholder": "",
        "required": false
      }
    }
];