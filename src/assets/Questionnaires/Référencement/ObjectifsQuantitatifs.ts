import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs quantitatifs";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "volume_trafic",
      "type": "textarea",
      "templateOptions": {
        "label": "Quel volume de trafic visez-vous sur votre site?",
        "placeholder": "",
        "required": false,
        "rows" : 2
      }
    },
    {
        "key": "nombre_mots_cles",
        "type": "input",
        "templateOptions": {
            "label": "Combien de mots-clés voulez-vous viser?",
            "placeholder": "",
            "type": "number",
        }
    },
    {
        "key": "volume_contact",
        "type": "textarea",
        "templateOptions": {
          "label": "Quel volume de contact visez-vous sur votre site?",
          "placeholder": "",
          "required": false,
          "rows" : 2
        }
      }
];