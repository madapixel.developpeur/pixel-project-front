import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Cibles";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "description_cible",
      "type": "textarea",
      "templateOptions": {
        "label": "Le site est-il pour les entreprises ou les particuliers?",
        "placeholder": "",
        "required": false,
        "rows" : 5
      }
    },
    {
        "key": "description_cible_precis",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous les décrire avec précision? (âge, habitudes, comportements, etc.)",
            "placeholder": "",
            "rows" : 10
        }
    }
];