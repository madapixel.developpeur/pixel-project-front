import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Périmètre du projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
      "key": "actions_projet",
      "type": "select",
      "templateOptions": {
        "label": "Le site doit-il être multilingue? Quelles autres actions souhaitez-vous appliquer sur le site?",
        "placeholder": "",
        "required": false,
        "multiple" : true,
          "labelProp": "label",
          "valueProp": (o : any) => o.label,
          "compareWith": (o1 : any, o2 : any) => o1 === o2,
          "options": [
            { "value": 1, "label": "Audit SEO (on-site / off-site)"  },
            { "value": 2, "label": "Optimisation du site (on-site / off-site)"  },
            { "value": 3, "label": "Mise en place d’une stratégie SEO"  },
            { "value": 4, "label": "Campagne de netlinking"  },
            { "value": 5, "label": "Campagne SEA"  }
          ]
      }
    }
];