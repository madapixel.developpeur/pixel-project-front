import { FormlyFieldConfig } from "@ngx-formly/core";

export const TITLE : string = "Complétez les coordonnées à propos de votre entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "name",
        "type": "input",
        "templateOptions": {
            "label": "What is your name?",
            "placeholder": "Enter your name"
        }
    },
    {
      "key": "birthdate",
      "type": "datepicker",
      "templateOptions": {
        "label": "What is your birthdate?",
        "placeholder": "Select your birthdate",
        "required": false
      }
    },
    {
        "key": "age",
        "type": "input",
        "templateOptions": {
            "label": "How old are you?",
            "type": "number",
            "placeholder": "Enter your age"
        }
    },
    {
        "key": "description",
        "type": "textarea",
        "templateOptions": {
            "label": "Votre Description",
            "type": "file",
            "placeholder": "Entrez votre Description",
            "rows":10
        }
    },
    {
        "key": "optionValue",
        "type": "select",
        "templateOptions": {
          "label": "Select 1",
          "labelProp": "label",
          "valueProp": (o : any) => o.value,
          "compareWith": (o1 : any, o2 : any) => o1 === o2,
          "options": [
            { "value": 1, "label": "Option 1"  },
            { "value": 2, "label": "Option 2"  },
            { "value": 3, "label": "Option 3"  },
            { "value": 4, "label": "Option 4"  }
          ]
        }
      },
    {
        "key": "gender",
        "type": "radio",
        "templateOptions": {
            "label": "What is your gender?",
            "options": [
            { "label": "Male", "value": "male" },
            { "label": "Female", "value": "female" }
            ]
        }
    },
    {
        "key": "newsletter",
        "type": "checkbox",
        "templateOptions": {
            "label": "Do you want to receive our newsletter?"
        }
    }
];