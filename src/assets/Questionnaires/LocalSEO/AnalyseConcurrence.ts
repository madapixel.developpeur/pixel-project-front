import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Secteur d'activité et géolocalisation";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents en ligne ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "positions_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment se positionnent-ils dans les résultats de recherche locaux ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "concurrents_referencement",
        "type": "textarea",
        "templateOptions": {
            "label": "Que font-ils en termes de référencement local que vous pourriez également mettre en œuvre ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];