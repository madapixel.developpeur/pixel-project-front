import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Secteur d'activité et géolocalisation";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "site_web",
        "type": "input",
        "templateOptions": {
            "label": "Avez-vous un site Web pour votre entreprise ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "google_my_business",
        "type": "input",
        "templateOptions": {
            "label": "Avez-vous déjà une fiche Google My Business ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "pages_reseaux_sociaux",
        "type": "input",
        "templateOptions": {
            "label": "Avez-vous des pages de réseaux sociaux pour votre entreprise ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "utilisation_siteweb_et_reseaux",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment utilisez-vous actuellement votre site Web et vos pages de réseaux sociaux pour atteindre vos objectifs de référencement local ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];