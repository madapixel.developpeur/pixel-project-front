import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs et attentes";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_pour_seo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont vos objectifs à court et à long terme pour le SEO local ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "attentes_specifiques",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des attentes spécifiques quant aux résultats du référencement local ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];