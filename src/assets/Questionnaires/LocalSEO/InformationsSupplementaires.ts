import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Informations supplémentaires concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "nom",
        "type": "input",
        "templateOptions": {
            "label": "Nom du projet", 
            "placeholder": ""
        }
    },{
        "key": "description",
        "type": "textarea",
        "templateOptions": {
            "label": "Description du projet",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    {
        "key": "fichiers",
        "type": "file",
        "templateOptions": {
            "label": "Fichiers supplémentaires",
            "placeholder": "",
            "multiple": true
        }
    },
];