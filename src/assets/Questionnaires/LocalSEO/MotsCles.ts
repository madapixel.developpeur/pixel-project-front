import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Stratégies de référencement : Mots-clés";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "mots_cles_recherche_locale",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels mots clés sont importants pour votre entreprise en termes de recherche locale ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "mots_cles_pertinents",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots-clés qui ont une forte pertinence pour votre entreprise dans ces régionsgéographiques ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];