import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Secteur d'activité et géolocalisation";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "secteur_activite",
        "type": "input",
        "templateOptions": {
            "label": "Dans quel secteur d'activité opérez-vous ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "regions_ameliorations_visibilite",
        "type": "textarea",
        "templateOptions": {
            "label": "Dans quelles régions géographiques souhaitez-vous améliorer votre visibilité en ligne ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "magasins_regions",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des magasins physiques ou des emplacements de bureaux dans ces régions géographiques ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];