import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Stratégies de référencement : Contenu";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "type_contenu_actuel",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel type de contenu est actuellement présent sur votre site Web ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "sujets_a_aborder",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les sujets que vous souhaitez aborder dans le contenu de votre site Web pour améliorer votre référencement local ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];