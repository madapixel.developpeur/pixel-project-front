import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Promotion et visibilité de l'application";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "canaux_de_promotion",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les canaux de promotion que vous utilisez actuellement pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "objectifs_visibilite",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont vos objectifs en matière de visibilité et de promotion pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "mesure_efficacite",
        "type": "input",
        "templateOptions": {
            "label": "Comment mesurez-vous l'efficacité de vos efforts de promotion actuels ?",
            "placeholder": "",
            "required": false
        }
    },
    
];