import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse de la concurrence";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "concurrents_directs",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des concurrents directs ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "description_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous nommer vos concurrents directs ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "analyse_concurrence",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà effectué une analyse de la concurrence et/ou une recherche de mots-clés pour votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    
];