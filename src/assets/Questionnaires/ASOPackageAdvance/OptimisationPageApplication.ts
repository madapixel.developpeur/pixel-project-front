import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Optimisation de la page de l'application";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "presentation_dans_app_store",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment la page de votre application est-elle actuellement présentée dans l'App Store ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    {
        "key": "amelioration_presentation",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment pouvez-vous améliorer la présentation de la page de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 6
        }
    },
    {
        "key": "principaux_avantages",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les principaux avantages de votre application que vous souhaitez mettre en avant dans la description ? ",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    
];