import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Analyse de l'application";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_application_et_objectifs",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est votre application et quel est son objectif ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "public_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est le public cible de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "canaux_de_distribution",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les canaux de distribution de votre application (App Store, Google Play, etc.) ?",
            "placeholder": "",
            "required": false,
            "rows": 2
        }
    },
    {
        "key": "analyse_application",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà effectué une analyse de votre application ? Si oui, pouvez-vous nous fournir es résultats ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    {
        "key": "principales_fonctionnalites",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les principales fonctionnalités de votre application et comment sont-elles mises en avant dans votre liste de fonctionnalités ?",
            "placeholder": "",
            "required": false,
            "rows": 6
        }
    },
];