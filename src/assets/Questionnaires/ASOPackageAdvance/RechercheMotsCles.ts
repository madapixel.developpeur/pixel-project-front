import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Recherche de mots clés";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "recherche_application",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment les utilisateurs potentiels recherchent-ils votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 3
        }
    },
    {
        "key": "resultats_recherches_mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà effectué une recherche de mots clés pour votre application ? Si oui, pouvez-vous nous fournir les résultats ? ",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    {
        "key": "optimisation_mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "A votre avis, comment vos mots clés peuvent-ils être optimisés pour améliorer la visibilité de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 4
        }
    },
    
];