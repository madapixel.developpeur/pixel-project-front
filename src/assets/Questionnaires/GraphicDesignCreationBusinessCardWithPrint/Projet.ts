import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "cibles",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui est votre public cible pour cette carte de visite ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "informations_essentielles",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les informations essentielles à inclure sur la carte de visite ?",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "idee_design",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous une idée du design ou de l’apparence générale que vous souhaitez pour votre carte de visite ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "quantite_a_produire",
        "type": "input",
        "templateOptions": {
            "label": "Quelle est la quantité de cartes de visite dont vous avez besoin ?",
            "placeholder": "",
            "type":"number",
            "required": false
        }
    },
    
];