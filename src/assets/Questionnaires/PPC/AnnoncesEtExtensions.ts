import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Recherche de mots clés et de groupes d'annonces";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "annonces",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà créé des annonces pour votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "type_annonce",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les types d'annonces que vous souhaitez utiliser pour votre campagne de publicité payante (ex. annonces textuelles, annonces vidéo, annonces display, etc.) ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "extensions_annonces",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà créé des extensions d'annonces pour votre campagne de publicité payante ? ",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];