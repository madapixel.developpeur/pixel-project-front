import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concurrents";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents et comment se positionnent-ils sur le marché ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "publicite_payante_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Ont-ils déjà mis en place des campagnes de publicité payante ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "differenciation_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment allez-vous vous différencier de vos concurrents à travers votre campagne de publicité payante ? ",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
];