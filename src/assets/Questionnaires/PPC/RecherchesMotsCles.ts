import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Recherche de mots clés et de groupes d'annonces";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "recherche_mots_cles",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà effectué une recherche de mots clés pour votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "mots_cles_pertinents",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots clés qui sont les plus pertinents pour votre campagne de publicité payante ? ",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "organisation",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment souhaitez-vous organiser vos groupes d'annonces pour votre campagne de publicité payante ? ",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];