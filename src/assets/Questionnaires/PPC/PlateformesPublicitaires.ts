import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Plateformes publicitaires";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "plateformes_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelles sont les plateformes publicitaires que vous souhaitez utiliser pour votre campagne de publicité payante (ex. Google Ads, Facebook Ads, LinkedIn Ads, etc.) ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "campagnes_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà créé des campagnes publicitaires sur ces plateformes ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "budget",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous un budget spécifique pour votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
];