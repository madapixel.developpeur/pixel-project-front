import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Suivi des conversions et des performances";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "indicateurs_de_performance",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les indicateurs clés de performance que vous souhaitez suivre pour votre campagne de publicité payante (ex. le taux de clics, le taux de conversion, le coût par conversion, etc.) ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "utilisation_donnees",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment allez-vous utiliser les données de performance pour ajuster et améliorer votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];