import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Périmètre du projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "messages_a_transmettre",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les messages clés que vous souhaitez transmettre à travers vos publicités ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "medias_campagne_publicite",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des images, des vidéos ou des créations publicitaires spécifiques que vous souhaitez utiliser pour votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
];