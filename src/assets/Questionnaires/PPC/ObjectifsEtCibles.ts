import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs et cibles";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_commerciaux",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont vos objectifs commerciaux pour votre campagne de publicité payante ? (ex. augmenter les ventes, augmenter les inscriptions, augmenter le trafic, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "produits_a_promouvoir",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les produits ou services que vous souhaitez promouvoir à travers votre campagne de publicité payante ?",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "public_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est votre public cible ? Quel est le profil type de votre client idéal ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
];