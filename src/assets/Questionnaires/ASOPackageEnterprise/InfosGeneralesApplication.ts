import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Informations générales sur l'application";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_application",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous décrire brièvement votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 8
        }
    },
    {
        "key": "objectifs",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les objectifs de votre application ?",
            "placeholder": "",
            "required": false,
            "rows": 5
        }
    },
    
];