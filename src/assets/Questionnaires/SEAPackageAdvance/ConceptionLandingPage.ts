import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Conception du Landing page";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "landing_page_actuel",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà créé un landing page pour votre campagne publicitaire Google Ads ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "objectifs_landing_page",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les objectifs que vous souhaitez atteindre avec votre landing page ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "idee_conception_landing_page",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous une idée de la conception ou de l'apparence que vous souhaitez pour votre Landing page ?",
            "placeholder": "",
            "rows":10,
            "required": false
        }
    },
    {
        "key": "elements_a_inclure",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les éléments que vous aimeriez inclure sur votre Landing page, tels que des formulaires de contact, des témoignages de clients, des vidéos, des images, etc. ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
   

];