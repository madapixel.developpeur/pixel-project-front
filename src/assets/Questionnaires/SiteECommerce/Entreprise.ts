import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Complétez les coordonnées à propos de votre entreprise";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "adresse",
        "type": "input",
        "templateOptions": {
            "label": "Adresse",
            "placeholder": "Entrez votre Adresse",
            "required": false
        }
    },{
        "key": "contact",
        "type": "input",
        "templateOptions": {
            "label": "Contact",
            "placeholder": "Entrez Contact",
            "required": false
        }
    },{
      "key": "date_de_creation",
      "type": "datepicker",
      "templateOptions": {
        "label": "Date de création",
        "placeholder": "Entrez la date de création de l'entreprise",
        "required": false
      }
    },
    {
        "key": "activite_principale",
        "type": "input",
        "templateOptions": {
            "label": "Activité principale",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "lien_reseaux_sociaux",
        "type": "textarea",
        "templateOptions": {
            "label": "Lien réseaux sociaux",
            "placeholder": "Entrez le(s) lien(s) réseaux sociaux'",
            "required": false,
            "rows":5
        }
    },
    {
        "key": "nom_domaine",
        "type": "input",
        "templateOptions": {
            "label": "Nom de domaine",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "serveur_hebergement",
        "type": "input",
        "templateOptions": {
            "label": "Serveur d’hébergement",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "conditions_generales_de_vente",
        "type": "file",
        "templateOptions": {
            "label": "Conditions générales de vente (.pdf)",
            "placeholder": "Entrez le conditions générales de vente",
            "multiple": true,
            "accept" : ".pdf"
        }
    },
    {
        "key": "mentions_legales",
        "type": "file",
        "templateOptions": {
            "label": "Mentions légales (.pdf)",
            "placeholder": "Entrez le mentions légales",
            "accept" : ".pdf"
        }
    },
    {
        "key": "nombre_de_salaries",
        "type": "input",
        "templateOptions": {
            "label": "Nombre de salariés",
            "type": "number",
            "placeholder": "Entrez le nombre de salariés"
        }
    }
];