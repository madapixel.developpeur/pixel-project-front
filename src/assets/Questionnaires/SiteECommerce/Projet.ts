import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concernant le projet";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "description_du_projet",
        "type": "textarea",
        "templateOptions": {
            "label": "Description du projet",
            "placeholder": "Entrez description du projet",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "objectif_du_site",
        "type": "textarea",
        "templateOptions": {
            "label": "Objectif du site",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "problemes_rencontres",
        "type": "textarea",
        "templateOptions": {
            "label": "problème(s) rencontré(s) - solution",
            "placeholder": "",
            "rows":4,
            "required": false
        }
    },
    {
        "key": "temoignagnes",
        "type": "textarea",
        "templateOptions": {
            "label": "Témoignages client",
            "placeholder": "",
            "rows":8,
            "required": false
        }
    },
    {
        "key": "questions_frequemment_posees",
        "type": "textarea",
        "templateOptions": {
            "label": "Lister 5 questions fréquemment posées",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "offres_tunnel",
        "type": "textarea",
        "templateOptions": {
            "label": "Décrire l’offre du tunnel (catalogue - mono produit, plusieurs produits; prix, nom de la référence, produit assujetti à la TVA)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "modele_facture",
        "type": "file",
        "templateOptions": {
            "label": "Télécharger un exemple de facture - préparer un modèle de facture (.pdf)",
            "placeholder": "",
            "multiple" : true,
            "accept" : ".pdf"
        }
    },
    {
        "key": "nombre_de_pages",
        "type": "input",
        "templateOptions": {
            "label": "Nombre de pages",
            "type": "number",
            "placeholder": ""
        }
    },
    {
        "key": "intitules_pages",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel serait l’intitulé de ces pages? (barre de navigation ou entrée de menu) Ex : accueil - nos services - contact, etc.",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "elements_redactionnels",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous transmettre les éléments rédactionnels concernant chacune de ces pages?",
            "multiple" : true,
            "placeholder": ""
        }
    },
    {
        "key": "etude_de_marche",
        "type": "file",
        "templateOptions": {
            "label": "Avez-vous réalisé une étude de marché? Si oui, merci de nous transmettre les éléments de l’étude.",
            "multiple" : true,
            "placeholder": ""
        }
    },
    {
        "key": "cibles",
        "type": "radio",
        "templateOptions": {
            "label": "Avez-vous identifié votre cible?",
            "options": [
            { "label": "Oui", "value": "Oui" },
            { "label": "Non", "value": "Non" }
            ]
        }
    },
    {
        "key": "description_clientele",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous décrire votre clientèle de façon précise et détaillée? (âge, homme/femme, communauté, catégorie socio-professionnel, situation matrimoniale, comportement, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "contexte_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Dans quel contexte votre entreprise a-t-elle été créée? (motivation, but)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "services_entreprise",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les différents services/produits/prestations fournis par votre entreprise?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "histoire_logo",
        "type": "textarea",
        "templateOptions": {
            "label": "Quelle est l’histoire derrière votre logo? Quelle est sa signification?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "referencement_actuel_site",
        "type": "textarea",
        "templateOptions": {
            "label": "Prend-on en compte le référencement de votre site internet ? Si oui, quelles sont les spécificités et objectifs attendus ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "objectifs_attendus_refonte",
        "type": "select",
        "templateOptions": {
          "label": "Pouvez-vous nous dire quels peuvent être les objectifs attendus post-refonte?",
          "placeholder": "",
          "required": false,
          "multiple" : true,
            "labelProp": "label",
            "valueProp": (o : any) => o.label,
            "compareWith": (o1 : any, o2 : any) => o1 === o2,
            "options": [
              { "value": 1, "label": "Ton de communication souhaitée"  },
              { "value": 2, "label": "Liste “do” - “don’t”"  },
              { "value": 3, "label": "Fonctionnalités à ajouter"  },
              { "value": 4, "label": "Recherche de nouveau client ET/OU fidélisation"  }
            ]
        }
      },
      {
        "key": "medias",
        "type": "file",
        "templateOptions": {
            "label": "Pouvez-vous nous fournir les photos/vidéos/fichiers ou tout éléments qui concernent votre entreprise et qui n’ont pas été mentionnés précédemment? (fichiers)",
            "placeholder": "",
            "multiple": true
        }
    },
    {
        "key": "systeme_paiement",
        "type": "textarea",
        "templateOptions": {
            "label": "Pouvez-vous nous en dire plus sur le système de paiement que vous souhaitez?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
];