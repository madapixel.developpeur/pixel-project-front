import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Concurrence";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "principaux_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Qui sont vos principaux concurrents en ligne ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "strategie_concurrente",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment vos concurrents utilisent-ils le SEM pour promouvoir leurs produits ou services ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "mots_cles_concurrents",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots clés que vos concurrents ciblent dans leurs campagnes de SEM ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];