import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Site web";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "etat_actuel_siteweb",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est l'état actuel de votre site web en termes de convivialité, de vitesse de chargement, de qualité du contenu, de structure de la page, etc. ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "siteweb_conversions",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous déjà un site web optimisé pour les conversions ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
    {
        "key": "pages_de_destinations",
        "type": "textarea",
        "templateOptions": {
            "label": "Avez-vous des pages de destination spécifiques pour vos campagnes de SEM ?",
            "placeholder": "",
            "rows":2,
            "required": false
        }
    },
];