import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Conception des annonces";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "formats_publicitaires",
        "type": "textarea",
        "templateOptions": {
            "label": "Souhaitez-vous créer des annonces textuelles, des annonces display, des annonces vidéo ou une combinaison de ces formats publicitaires ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "elements_cles_a_inclure",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les éléments clés que vous souhaitez inclure dans vos annonces (ex. des offres spéciales, des appels à l'action, des images, des témoignages clients, etc.) ?",
            "placeholder": "",
            "rows":6,
            "required": false
        }
    },
    {
        "key": "utilisations_donnees_de_performance",
        "type": "textarea",
        "templateOptions": {
            "label": "Comment allez-vous utiliser les données de performance pour ajuster et améliorer vos campagnes de SEM ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];