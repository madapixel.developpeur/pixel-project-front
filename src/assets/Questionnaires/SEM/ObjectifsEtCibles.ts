import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Objectifs et cibles";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_pour_marketing",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont vos objectifs de marketing en ligne ? (ex. augmenter les ventes, augmenter la notoriété de la marque, etc.)",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "produits_et_services_a_promouvoir",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les produits ou services que vous souhaitez promouvoir à travers le SEM ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "public_cible",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est votre public cible ? Quel est le profil type de votre client idéal ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
    {
        "key": "mots_cles_campagnes",
        "type": "textarea",
        "templateOptions": {
            "label": "Quels sont les mots clés que vous souhaitez utiliser pour vos campagnes de SEM ?",
            "placeholder": "",
            "rows":5,
            "required": false
        }
    },
];