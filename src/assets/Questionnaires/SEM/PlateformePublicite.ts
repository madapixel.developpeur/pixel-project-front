import { FormlyFieldConfig } from "@ngx-formly/core";
import { of } from "rxjs";

export const TITLE : string = "Plateforme de publicité";
export const FORM_CONFIG: FormlyFieldConfig[] = [
    {
        "key": "objectifs_pour_marketing",
        "type": "input",
        "templateOptions": {
            "label": "Avez-vous déjà une plateforme de publicité en ligne que vous utilisez actuellement ?",
            "placeholder": "",
            "required": false
        }
    },
    {
        "key": "choix_moyens",
        "type": "textarea",
        "templateOptions": {
            "label": "Souhaitez-vous que nous utilisions Google Ads ou une autre plateforme publicitaire ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
    {
        "key": "niveaux_connaissance_sem",
        "type": "textarea",
        "templateOptions": {
            "label": "Quel est votre niveau de connaissance et d'expérience en matière de SEM ?",
            "placeholder": "",
            "rows":3,
            "required": false
        }
    },
];