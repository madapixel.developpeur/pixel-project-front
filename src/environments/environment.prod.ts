export const environment = {
  production: true,
  baseUrl : "https://techleadgenius.fr/",
  wsUrl : "https://back.techleadgenius.fr/",
  pipe : {
    date : 'medium',
    number : '0.2-2'
  },
  currency : 'Ar',
  status : {
    deleted : -1,
    created : 0,
    validated : 1
  },
  carStatus : {
    inCirculation : 0, 
    deposited : 1,
    inReparation: 2,
    waitExit : 3,
  },
  repairStatus : {
    todo : 0,
    inprogress : 1,
    ended : 2
  },
  todoStatus: {
    todo: 0,
    inProgress: 1,
    finished: 2,
    validated: 3,
  },
  tva : 20,
  roles : {
    "1" : "Client",
    "2" : "Administrateur",
  },
  typeClient: {
    particulier: 1,
    societe: 2
  },
  allRoles : {
    client: 1,
    admin: 2,
  },
  projectStatus: {
    deleted : -1,
    creating : 0,
    created : 1,
    updated: 2,
    validatedByAdmin : 3,
    todosValidatedByClient: 4,
    finished: 5,
    billed: 6
  }
  // baseUrl : "http://localhost:4200",
  // wsUrl : "http://localhost:3000",
};
